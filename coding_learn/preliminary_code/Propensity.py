import numpy
import sklearn.linear_model
import sys


def CS_LR_PROPENSITIES(user_features, item_features, observation_matrix, verbose = False):
    numUsers, numUserFeatures = numpy.shape(user_features)
    numItems, numItemFeatures = numpy.shape(item_features)
    
    designMatrix = numpy.empty((numUsers * numItems, numUserFeatures + numItemFeatures + 
                                    numUserFeatures*numItemFeatures + numUsers + numItems), dtype = numpy.int)
    targets = numpy.empty(numUsers * numItems, dtype = numpy.int)
    
    currID = -1
    for user in range(numUsers):
        for item in range(numItems):
            currID += 1
            designMatrix[currID, 0 : numUserFeatures] = user_features[user, :]
            designMatrix[currID, numUserFeatures : numUserFeatures+numItemFeatures] = item_features[item, :]
            designMatrix[currID, numUserFeatures+numItemFeatures : numUserFeatures+numItemFeatures + \
                                                                    numUserFeatures*numItemFeatures] =\
                            numpy.kron(user_features[user, :], item_features[item, :])
            designMatrix[currID, numUserFeatures+numItemFeatures + numUserFeatures*numItemFeatures +user] = 100
            designMatrix[currID, numUserFeatures+numItemFeatures + numUserFeatures*numItemFeatures +numUsers +item]\
                                                                                                          = 100
            targets[currID] = observation_matrix[user, item]
            
    if verbose:
        print "Propensity.CS_LR_PROPENSITIES: [LOG]\t Num[Users/Items/UFeatures/IFeatures]", \
            numUsers, numItems, numUserFeatures, numItemFeatures
            
    learner = sklearn.linear_model.LogisticRegressionCV(Cs = [1e+5,1e+4,1e+3,1e+2,1e+1,1,1e-1],
            fit_intercept = True, cv = 4, penalty = 'l2', dual = False,
            scoring = "log_loss", solver = 'liblinear', intercept_scaling = 100, tol = 1e-7, max_iter = 2000,
            n_jobs = -1, verbose = 0, refit = True)
            
    learner.fit(designMatrix, targets)
    learnedPropensities = learner.predict_proba(designMatrix)
    learnedPropensities = numpy.reshape(learnedPropensities[:,1], (numUsers, numItems))

    if verbose:    
        print "Propensity.CS_LR_PROPENSITIES: [DBG]\t Learned propensities:",\
                learnedPropensities.sum(dtype = numpy.longdouble),\
                observation_matrix.sum(dtype = numpy.longdouble)
    
    perUserObservations = observation_matrix.sum(axis = 1, dtype = numpy.longdouble)
    perUserProbability = 24.0 * numpy.reciprocal(perUserObservations)       #Only applicable for the COAT dataset
    perUserProbability = numpy.clip(perUserProbability, a_min = 0, a_max = 1)
    
    propensities = numpy.multiply(learnedPropensities, perUserProbability[:,None])
    
    print "Propensity.CS_LR_PROPENSITIES: [DBG]\t Final propensities:",\
                propensities.sum(axis = 1, dtype = numpy.longdouble)
    
    return propensities


def NAIVE_BAYES_PROPENSITIES(observed_ratings, rating_marginals, verbose = False):
    numObservations = numpy.ma.count(observed_ratings)
    numUsers, numItems = numpy.shape(observed_ratings)
    scale = numUsers * numItems
    if verbose:
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [LOG]\t (NumUsers,NumItems,NumObservations):", \
            numUsers, numItems, numObservations
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [LOG]\t Rating marginals", rating_marginals

    if numpy.any(rating_marginals <= 0.0):
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [ERR]\t Some rating marginals are 0:", rating_marginals
        sys.exit(0)

    flatObservations = numpy.ma.compressed(observed_ratings)
    ratingRange = numpy.shape(rating_marginals)[0]
    
    observationProbability = 1.0 * numObservations / scale
    empiricalCounts = numpy.bincount(flatObservations, minlength = ratingRange + 1)[1:]
    empiricalRatingDistribution = 1.0 * (empiricalCounts + 1) / (numObservations + ratingRange)     #LaplaceSmoothing

    if verbose:
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [DBG]\t Empirical rating counts/distribution",\
                    empiricalCounts, empiricalRatingDistribution

    propensityPerRating = observationProbability * numpy.divide(empiricalRatingDistribution, rating_marginals)

    if verbose:
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [DBG]\t Unclipped Propensity per rating", propensityPerRating

    propensityPerRating = numpy.clip(propensityPerRating, a_min = 0, a_max = 1)

    if verbose:
        print "Propensity.NAIVE_BAYES_PROPENSITIES: [DBG]\t Inverse propensities", numpy.reciprocal(propensityPerRating)

    return propensityPerRating

 
def PARTIAL_OBSERVE(complete_matrix, alpha, sparsity, verbose = False):
    ratingRange = range(int(complete_matrix.min()), int(complete_matrix.max())+1)
    numRatings = len(ratingRange)
    ratingDistribution = numpy.bincount(complete_matrix.ravel(), minlength = numRatings + 1)[1:]
    if verbose:
        print "Propensity.PARTIAL_OBSERVE: [LOG]\t Histogram of complete ratings:", ratingDistribution, \
            "\t Alpha:", alpha, "\t Sparsity:", sparsity
            
    ratingPropensities = numpy.zeros(numRatings, dtype = numpy.longdouble)
    factor = 1.0
    for i in range(numRatings):
        ratingPropensities[numRatings - i - 1] = factor
        if i > 0:
            factor *= alpha
    
    numUsers, numItems = numpy.shape(complete_matrix)
    numObservations = sparsity * numUsers * numItems
    
    expectedObservations = numpy.dot(ratingDistribution, ratingPropensities)
    
    scale = numObservations * 1.0 / expectedObservations
    ratingPropensities = numpy.clip(scale * ratingPropensities, a_min = 0, a_max = 1)
    
    if verbose:
        print "Propensity.PARTIAL_OBSERVE: [DBG]\t PropensityPerRating:", ratingPropensities, \
            "\t NumObservations/ExpectedObservations[Unclipped,Corrected]:", numObservations, \
                    expectedObservations, numpy.dot(ratingDistribution, ratingPropensities)

    propensities = numpy.zeros((numUsers, numItems), dtype = numpy.longdouble)
    for i in range(numRatings):
        propensities[complete_matrix == ratingRange[i]] = ratingPropensities[i] 

    randomDraw = numpy.random.random((numUsers, numItems))
    observed = randomDraw < propensities

    observedRatings = numpy.ma.array(complete_matrix, dtype = numpy.int, copy = True, 
                            mask = randomDraw >= propensities, fill_value = 0, hard_mask = True)
 
    return observedRatings, propensities
 
