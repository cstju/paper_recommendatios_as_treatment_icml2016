import argparse
import itertools
import os
import sys

import Expt2
import MF
import Metrics
import numpy
from joblib import Parallel, delayed


class Files(object):
    def __init__(self, train, test, propensities):
        self.train = train
        self.test = test
        self.propensities = propensities


class Logger(object):
    def __init__(self, verbosity_level=1):
        self._verbosity_level = verbosity_level

    def log(self, message, level=1):
        if level <= self._verbosity_level:
            print message
            sys.stdout.flush()


def learn(data, logger, lambdas=None, seed=None, numdims=None, approach=None, metric=None, raw_metric=None,
          output_name=None, propensities_desc=None):
    clipVals = [-1]
    biasModes = ['Free', 'Regularized']
    numpy.random.seed(seed)

    numBiasModes = len(biasModes)
    numLambdas = len(lambdas)
    numDimSettings = len(numDims)
    numClipSettings = len(clipVals)
    numParamSettings = numLambdas * numDimSettings * numClipSettings * numBiasModes

    paramSettings = list(itertools.product(lambdas, numDims, clipVals, biasModes))
    numApproaches = 1

    selfMatrix, unifMatrix = data.train, data.test
    numUsers = unifMatrix.shape[0]
    unifIndices = list(range(numUsers))

    logger.log("Starting learning...")
    logger.log("\t-metric: " + raw_metric, 2)
    logger.log("\t-lambda values: " + str(lambdas), 2)
    logger.log("\t-dimension values: " + str(numdims), 2)
    logger.log("\t-propensity scoring method: " + propensities_desc)
    if data.propensities is not None:
        invP = numpy.reciprocal(data.propensities)
        invP = numpy.ma.array(invP, dtype=numpy.longdouble, copy=False,
                              mask=numpy.ma.getmask(selfMatrix), fill_value=0, hard_mask=True)
    else:
        invP = None

    foldScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype=numpy.float)
    foldTestScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype=numpy.float)

    observationIndices = numpy.ma.nonzero(selfMatrix)
    numObservations = numpy.ma.count(selfMatrix)

    shuffleIndices = numpy.random.permutation(numObservations)
    fractionObservations = int(numObservations / 4)
    firstFold = shuffleIndices[:fractionObservations]
    secondFold = shuffleIndices[fractionObservations:2 * fractionObservations]
    thirdFold = shuffleIndices[2 * fractionObservations:3 * fractionObservations]
    fourthFold = shuffleIndices[3 * fractionObservations:]

    logger.log("Split %d observations into folds. Fold sizes: %s" %
               (len(shuffleIndices), str([len(firstFold), len(secondFold), len(thirdFold), len(fourthFold)])),
               2)

    for fold in xrange(4):
        logger.log("Learning on fold %d " % fold)
        trainObservations = numpy.ma.copy(selfMatrix)
        testObservations = numpy.ma.copy(selfMatrix)

        if fold == 0:
            trainObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                numpy.ma.masked

            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                numpy.ma.masked
        elif fold == 1:
            trainObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                numpy.ma.masked
        elif fold == 2:
            trainObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                numpy.ma.masked
        elif fold == 3:
            trainObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                numpy.ma.masked

        # Get starting params by SVD
        startTuple = Expt2.INIT_PARAMS(trainObservations, 40)
        normN = "Vanilla"
        approachIndex = 0

        modelsPerLambda = Parallel(n_jobs=-1, verbose=0)(delayed(Expt2.MF_TRAIN)(param,
                                                                                 trainObservations, invP, normN,
                                                                                 raw_metric, startTuple)
                                                         for param in paramSettings)

        for lambdaIndex, eachModel in enumerate(modelsPerLambda):
            selectedBiasMode = paramSettings[lambdaIndex][3]
            selectedBias = True
            if selectedBiasMode == 'None':
                selectedBias = False
            predictedY = MF.PREDICTED_SCORES(eachModel[0], eachModel[1],
                                             eachModel[2], eachModel[3], eachModel[4], use_bias=selectedBias)

            score = None
            if invP is not None:
                score = metric(testObservations, predictedY, 4.0 * invP)
            else:
                score = metric(testObservations, predictedY, invP)
            score = score[0]
            foldScores[approachIndex, fold, lambdaIndex] = score
            unifUserY = predictedY[unifIndices, :]

            foldTestScore = metric(unifMatrix, unifUserY, None)[0]
            foldTestScores[approachIndex, fold, lambdaIndex] = foldTestScore
            logger.log("\tLambda/NumDims: " + str(paramSettings[lambdaIndex]) +
                       ", Test Fold Score: " + str(score) + ", Test Set Score: " + str(foldTestScore), 2)

    eventualApproachParams = []

    normN = "Vanilla"
    approachIndex = 0
    approachScores = foldScores[approachIndex, :, :]
    allFoldScores = approachScores.sum(axis=0, dtype=numpy.float)
    bestLambdaIndex = numpy.argmin(allFoldScores)
    bestLambda = paramSettings[bestLambdaIndex]
    logger.log("Retraining with best hyperparameter values: " + str(bestLambda))
    logger.log("Chosen from average cross-validation performance:", 2)
    for everyLambdaIndex, everyLambda in enumerate(paramSettings):
        logger.log("\t" + str(everyLambda) + ": " +  str(allFoldScores[everyLambdaIndex]), 2)
    eventualApproachParams.append((approach, invP, normN, bestLambda))

    finalModels = Parallel(n_jobs=-1, verbose=0)(delayed(Expt2.FINAL_TRAIN)(approachTup,
                                                                            raw_metric, selfMatrix, startTuple)
                                                 for approachTup in eventualApproachParams)

    for approachID, approachTuple in enumerate(eventualApproachParams):
        resultTuple = finalModels[approachID]
        finalBiasMode = approachTuple[3][3]
        finalBias = True
        if finalBiasMode == 'None':
            finalBias = False

        predictedY = MF.PREDICTED_SCORES(resultTuple[0], resultTuple[1],
                                         resultTuple[2], resultTuple[3], resultTuple[4], use_bias=finalBias)
        numpy.savetxt(output_name, predictedY)
        unifUserY = predictedY[unifIndices, :]
        metricValue = metric(unifMatrix, unifUserY, None)[0]
        logger.log("Done. Final test set error: " + str(metricValue))


def load_ratings(filename):
    try:
        raw_matrix = numpy.loadtxt(filename)
        return numpy.ma.array(raw_matrix, dtype=numpy.int, copy=False,
                              mask=raw_matrix <= 0, fill_value=0, hard_mask=True)
    except:
        print "Error: Could not load rating file '%s'" % filename
        exit()


def load_propensities(filename):
    try:
        return numpy.loadtxt(filename)
    except:
        print "Error: Could not load propensities."
        exit()


def check_writeable(filename):
    try:
        with open(filename, "wb") as f:
            pass
        os.remove(filename)
        return True, ""
    except IOError:
        print "Error: Could not open file '%s' for writing" % filename
        exit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Propensity-scored Matrix Factorization.')
    parser.add_argument("--ratings", "-r", type=str,
                        help="ratings matrix in ASCII format")
    parser.add_argument("--test", "-t", type=str,
                        help="test ratings in ASCII format")
    parser.add_argument("--propensities", "-p", type=str, default="",
                        help="propensities matrix in ASCII format (optional)")
    parser.add_argument("--completed", "-c", type=str, default="completed_ratings.ascii",
                        help="filename for completed matrix")
    parser.add_argument('--metric', '-m', metavar='M', type=str, choices=["MSE", "MAE"],
                        help='Metric to be optimized', default='MSE')
    parser.add_argument('--lambdas', '-l', metavar='L', type=str,
                        help='Lambda values', default='0.008,0.04,0.2,1,5,25,125')
    parser.add_argument('--numdims', '-n', metavar='N', type=str,
                        help='Dimension values', default='5,10,20,40')
    parser.add_argument('--seed', '-s', metavar='S', type=int,
                        help='Seed for numpy.random', default=387)
    parser.add_argument("--verbosity", "-v", type=int, choices=[0, 1, 2],
                        help="output verbosity (default = 2)", default=2)

    args = parser.parse_args()
    check_writeable(args.completed)
    my_logger = Logger(args.verbosity)

    lambdas = []
    tokens = args.lambdas.strip().split(',')
    for token in tokens:
        lambdas.append(float(token))

    numDims = []
    tokens = args.numdims.strip().split(',')
    for token in tokens:
        numDims.append(int(token))

    train = load_ratings(args.ratings)
    test = load_ratings(args.test)
    if args.propensities:
        propensities = load_propensities(args.propensities)
        propensities_desc = "IPS using " + args.propensities
    else:
        propensities = None
        propensities_desc = "naive (uniform)"
    data = Files(train, test, propensities)

    if args.metric == 'MSE':
        metric = Metrics.MSE
    elif args.metric == 'MAE':
        metric = Metrics.MAE

    learn(data, my_logger, lambdas=lambdas, numdims=numDims, metric=metric, approach="IPS",
          seed=args.seed, raw_metric=args.metric, output_name=args.completed, propensities_desc=propensities_desc)
