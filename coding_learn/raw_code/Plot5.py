if __name__ == "__main__":
    import argparse
    import pickle
    import matplotlib
    #matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    import glob
    
    parser = argparse.ArgumentParser(description='Plotting results of Expt5.py.')
    parser.add_argument('--filepath', '-f', metavar='F', type=str, 
                        help='File name of pkl', default='../logs/expt5/387_*_MSE_*,*.pkl')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metric', default='MSE')
    parser.add_argument('--save', dest='save', action='store_true')
    parser.set_defaults(save=False)
    
    args = parser.parse_args()
    files = glob.glob(args.filepath)

    trials = []
    
    approaches = ['Naive','Gold-IPS','NB-IPS']
    approachDict = {}
    for approach in approaches:
        approachDict[(approach, args.metric)] = len(approachDict)

    color_string = 'rbm'        
    #titleString = 'Learning: Observational setting      '+args.metric
    plt.rc('font', size = 24)
    plt.rc('text', usetex=True)
    plt.rc('font', family = 'serif')
    #plt.suptitle(titleString)


    alphas = [1,1e-3,2e-4,4e-5,8e-6,0]
    numAlphas = len(alphas)
    alphas = numpy.array(alphas) * 1586126

    numApproaches = len(approachDict)
    
    results = {}
    for fileName in files:
        startIndex = fileName.index('387_') + 4
        endIndex = fileName.index('_', startIndex)
        trialID = int(fileName[startIndex:endIndex])
        if trialID not in results:
            results[trialID] = numpy.zeros((numApproaches, numAlphas), dtype = numpy.float)
            
        g = open(fileName, 'rb')
        currentApproaches, currentApproachDict, currentAlphas, currentEstimates = pickle.load(g)
        g.close()
        
        for eachApproach in currentApproaches:
            tupleKey = (eachApproach, args.metric)
            currentIndex = currentApproachDict[tupleKey]
            globalIndex = approachDict[tupleKey]
            
            results[trialID][globalIndex,:] = currentEstimates[currentIndex,:]
            
    allEstimates = numpy.dstack(results.values())
    numTrials = numpy.shape(allEstimates)[2]
    
    def PlotErrors(nowApproaches, output_file):
        #plt.clf()
        legendList = []
        for approachIndex,approach in enumerate(nowApproaches):
            approachTup = (approach, args.metric)
            approachInd = approachDict[approachTup]
                
            estimates = allEstimates[approachInd, :, :]
            perAlphaEstimates = numpy.mean(estimates, axis = 1, dtype = numpy.longdouble)
            perAlphaStd = numpy.std(estimates, axis = 1, dtype = numpy.longdouble)
            perAlphaStd = 2/numpy.sqrt(numTrials)*perAlphaStd
                
            if approach != 'NB-IPS':
                perAlphaEstimates[-1] = perAlphaEstimates[-2]
                perAlphaStd[-1] = perAlphaStd[-2]
            l = plt.plot(alphas, perAlphaEstimates, color=color_string[approachIndex], rasterized=True, linewidth=2.0)
            plt.fill_between(alphas, perAlphaEstimates - perAlphaStd, perAlphaEstimates + perAlphaStd,
                        color=l[0].get_color(), alpha=0.2, rasterized=True)
           
            if approach == 'Gold-IPS':
                legendList.append('MF-IPS')
            elif approach == 'NB-IPS':
                legendList.append('MF-IPS-NB')
            else:
                legendList.append('MF-Naive')

            plt.legend(legendList, loc='upper right')
            #plt.suptitle(args.metric)
            plt.xlabel(r'Number of MCAR ratings seen by NB')
            plt.ylabel(args.metric)
            ax = plt.gca()
            #ax.set_yscale("log")            
            ax.set_xlim([0, 1586126])
            #ax.set_ylim([0, 5])
            ax.set_xscale("symlog", linthreshx = alphas[-2])

        plt.tight_layout()
        if args.save:
            plt.savefig(outFile+'.png', format='png', dpi = 100)
            plt.savefig(outFile+'.pdf', format='pdf', dpi = 100)
        else:
            plt.show()
 
    outFile = './plot5_'+args.metric   
    approaches = ['Naive', 'Gold-IPS', 'NB-IPS']
    PlotErrors(approaches, outFile)
    """        
    outFile = './plot2_'+args.metric+'_snips.pdf'            
    approaches = ['Naive', 'Gold-SNIPS', 'NB-SNIPS']
    PlotErrors(approaches, outFile)
            
    outFile = './plot2_'+args.metric+'_unips.pdf'            
    approaches = ['Naive', 'Gold-UNIPS', 'NB-UNIPS']
    PlotErrors(approaches, outFile)
            
    outFile = './plot2_'+args.metric+'_inips.pdf'            
    approaches = ['Naive', 'Gold-INIPS', 'NB-INIPS']
    PlotErrors(approaches, outFile)        
    """
