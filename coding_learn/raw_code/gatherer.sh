#!/bin/bash

for IP in '128.84.8.58' '128.84.8.207'
do
    scp ubuntu@$IP:/home/ubuntu/adith/*.log /home/adith/nmar-factorization/logs/expt2/
    scp ubuntu@$IP:/home/ubuntu/logs/expt2/*.pkl /home/adith/nmar-factorization/logs/expt2/
done

for IP in '128.84.8.155' '128.84.8.50'
do
    scp ubuntu@$IP:/home/ubuntu/adith/*.log /home/adith/nmar-factorization/logs/expt5/
    scp ubuntu@$IP:/home/ubuntu/logs/expt5/*.pkl /home/adith/nmar-factorization/logs/expt5/
done

