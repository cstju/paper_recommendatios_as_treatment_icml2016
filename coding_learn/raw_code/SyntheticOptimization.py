if __name__ == "__main__":
    import argparse
    import MF
    import BMF
    import numpy
    import itertools

    parser = argparse.ArgumentParser(description='Recommendations on toy dataset.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--numDims', '-d', metavar='D', type=int, 
                        help='Dimension values', default='2')
    parser.add_argument('--maxDims', '-m', metavar='M', type=int, 
                        help='Dimension values for true ratings', default='4')
    parser.add_argument('--numRecs', '-k', metavar='K', type=int, 
                        help='Number of recommendations', default='1')
    parser.add_argument('--warmStart', '-w', metavar='W', type=bool, 
                        help='Warm start methods', default=False)
    parser.add_argument('--l2Lambda', '-l', metavar='L', type=float, 
                        help='Lambda value for L2 regularization', default='1e-6')
    
    numpy.set_printoptions(precision=2, suppress=True)
    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    userVectors = numpy.array(list(itertools.product([1,-1], repeat = args.maxDims)))
    itemVectors = numpy.array(list(itertools.product([1,0], repeat = args.maxDims)))
    rawRatings = numpy.dot(userVectors, itemVectors.T)
    numUsers, numItems = numpy.shape(rawRatings)
    
    predictedItems = numpy.zeros((numUsers, args.numRecs), dtype = numpy.float)
    startVec = None
    if args.warmStart:
        userStart = numpy.zeros((numUsers, args.numDims), dtype = numpy.float)
        itemStart = numpy.zeros((numItems, args.numDims), dtype = numpy.float)
        if args.numDims <= args.maxDims:
            userStart = userVectors[:,0:args.numDims]
            itemStart = itemVectors[:,0:args.numDims]
        else:
            userStart[:,0:args.maxDims] = userVectors
            itemStart[:,0:args.maxDims] = itemVectors
            
        startVec = (userStart, itemStart, numpy.zeros(numUsers), numpy.zeros(numItems), 0)
    
    sortedRatings = numpy.sort(rawRatings, axis = 1)
    for i in xrange(numUsers):
        predictedItems[i, :] = sortedRatings[i,-args.numRecs:]
        
    maxValue = numpy.mean(predictedItems, dtype = numpy.float)
    print " *** Max possible utility", maxValue
    
    resultTuple = MF.GENERATE_MATRIX(rawRatings, None, args.l2Lambda, args.numDims, 'Vanilla', bias_mode = 'Free', mode = 'MSE', start_vec = startVec)
    predictedY = MF.PREDICTED_SCORES(resultTuple[0],resultTuple[1],resultTuple[2],resultTuple[3],resultTuple[4], True)
    #print predictedY

    maxIndices = numpy.argsort(predictedY, axis = 1)
    predictedRatings = numpy.zeros((numUsers, args.numRecs), dtype = numpy.float)
    for i in xrange(numUsers):
        predictedRatings[i,:] = rawRatings[i,maxIndices[i,-args.numRecs:]]
    
    mfValue = numpy.mean(predictedRatings, dtype = numpy.float)
    print " *** MF utility", mfValue
    #print "MatrixFactorization"
    #print predictedY
    
    resultTuple = BMF.LEARN(rawRatings, None, args.numDims, args.numRecs, bias_mode = 'Free', start_vec = startVec, verbose = True)
    learntPolicy = BMF.PREDICTED_PROBABILITIES(resultTuple[0],resultTuple[1],resultTuple[2],resultTuple[3],resultTuple[4])
    print learntPolicy

    print "Learnt CLRS policy: Discrepancy in row sums", numpy.linalg.norm(learntPolicy.sum(axis=1) - args.numRecs)
    #print "Histogram of predicted probabilities:", numpy.histogram(learntPolicy, bins = 20)
    #print learntPolicy

    maxIndices = numpy.argsort(learntPolicy, axis = 1)
    predictedRatings = numpy.zeros((numUsers, args.numRecs), dtype = numpy.float)
    for i in xrange(numUsers):
        predictedRatings[i,:] = rawRatings[i,maxIndices[i,-args.numRecs:]]
    
    clrsValue = numpy.mean(predictedRatings, dtype = numpy.float)
    print " *** CLRS deterministic utility", clrsValue
    
    perUserRating = numpy.sum(numpy.multiply(learntPolicy, rawRatings), axis = 1, dtype = numpy.float) / args.numRecs
    print " *** CLRS utility", perUserRating.mean(dtype = numpy.float)
