if __name__ == "__main__":
    import argparse
    import glob
    import numpy
    import Metrics
    import pickle
    import MF
    import Datasets
    import sys
    import scipy.sparse
    import scipy.stats
    
    parser = argparse.ArgumentParser(description='Significance testing on Real world datasets.')
    parser.add_argument('--dataset', '-d', metavar='D', type=str, 
                        help='Dataset name', default='CS')
    parser.add_argument('--assumption', '-a', metavar='A', type=str, 
                        help='Baseline system assumption', default='HL_MNAR')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metric', default='MSE')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    
    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    currMetric = None
    if args.metric == 'MSE':
        currMetric = Metrics.MSE
    elif args.metric == 'MAE':
        currMetric = Metrics.MAE
    else:
        print "Sig_Test: [ERR] Unrecognized metric", metric
        sys.exit(0)    
        
    
    dims = [5,10,20,40]
    hl_dir = '../HL_predictions/'+args.dataset+'/'+args.assumption+'/results_'
    
    selfMatrix = None
    nbUnifMatrix = None
    unifMatrix = None
    lrPropensities = None
    tbsPropensities = None
    nbUnifIndices = None
    unifIndices = None
    
    if args.dataset == 'Yahoo':
        selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices = Datasets.YAHOO_R3('../', 0.05)
    elif args.dataset == 'CS':
        selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices, lrPropensities = Datasets.COAT_SHOP('../', 0, True)
        nbUnifMatrix = unifMatrix.copy()
        tbsPropensities = numpy.loadtxt('../data_real_users/results/lr_model/learned_propensities.ascii')
        print "Expt3: [LOG] Loaded learned propensities from data_real_users", numpy.shape(tbsPropensities)
        print "Expt3: [DBG]\t TBSPropensities User sum: ", numpy.sum(tbsPropensities, axis = 1, dtype = numpy.longdouble)
    else:
        print "Unrecognized dataset ", args.dataset
        sys.exit(0)
    
    outputFile = '../logs/expt3/'+str(args.seed)+'_'+args.metric+'_'+args.dataset.lower()+'_'
    finalResults = None
    ourPredictions = None
    if args.dataset == 'Yahoo':
        g = open(outputFile+'Naive,NB-IPS.pkl', 'rb')
        finalResults = pickle.load(g)
        g.close()
    else:
        g = open(outputFile+'Naive,LR-IPS,TBS-IPS.pkl', 'rb')
        finalResults = pickle.load(g)
        g.close()
    approaches = finalResults[0]
    approachDict = finalResults[1]
    allEstimates = finalResults[2]
    
    g = open(outputFile+ '_finalmodels.pkl', 'rb')
    finalModels = pickle.load(g)
    g.close()
    
    
    for approachID, approach in enumerate(approaches):
        approachTuple = (approach, args.metric)
        approachIndex = approachDict[approachTuple]
        
        resultTuple = finalModels[approachID]
        predictedY = MF.PREDICTED_SCORES(resultTuple[0],resultTuple[1],
                                    resultTuple[2],resultTuple[3],resultTuple[4])
        unifUserY = predictedY[unifIndices, :]
        
        if args.dataset == 'Yahoo' and approach == 'NB-IPS':
            ourPredictions = unifUserY
        if args.dataset == 'CS' and approach == 'TBS-IPS':
            ourPredictions = unifUserY
            
        metricValue = currMetric(unifMatrix, unifUserY, None)[0]
        
        print "Debug: ", metricValue, "\t", allEstimates[approachIndex]
    
    
    observations = numpy.where(unifMatrix > 0)
    numObservations = numpy.shape(observations[0])[0]
    numUsers, numItems = numpy.shape(unifMatrix)
    ourDelta = numpy.zeros((numUsers, numItems), dtype = numpy.float)
    ourDelta[observations] = ourPredictions[observations] - unifMatrix[observations]
    
    ourError = None
    if args.metric == 'MSE':
        ourError = numpy.square(ourDelta[observations])
    else:
        ourError = numpy.abs(ourDelta[observations])
    
    
    for dim in dims:
        files = glob.glob(hl_dir + r'*_' + str(dim)+'/testPredictions.txt')
        numTrials = len(files)
        userIndices = None
        itemIndices = None
        allVals = None
        
        for file in files:
            rows, cols, vals = numpy.loadtxt(file, usecols=(0,1,3), unpack = True)
            if userIndices is None:
                userIndices = rows - 1
            if itemIndices is None:
                itemIndices = cols - 1
            if allVals is None:
                allVals = vals
            else:
                allVals += vals
        allVals = allVals / numTrials

        hlPredictions = scipy.sparse.coo_matrix((allVals, (userIndices,itemIndices)),
                        dtype = numpy.float)
        hlPredictions = hlPredictions.toarray()
        unifUserY = hlPredictions[unifIndices, :]
        metricValue = currMetric(unifMatrix, unifUserY, None)[0]
        
        theirDelta = numpy.zeros((numUsers, numItems), dtype = numpy.float)
        theirDelta[observations] = unifUserY[observations] - unifMatrix[observations]
        theirError = None
        if args.metric == 'MSE':
            theirError = numpy.square(theirDelta[observations])
        else:
            theirError = numpy.abs(theirDelta[observations])
        
        t,p = scipy.stats.ttest_rel(ourError, theirError, axis = None)
        
        print "Dim ", dim, "Trials ", numTrials, "Score ", metricValue, "t/p: ", t, p/2
