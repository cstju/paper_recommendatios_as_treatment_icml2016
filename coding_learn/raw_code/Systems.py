import numpy


def REC_ONES(true_ratings, verbose = False):
    predictedRatings = true_ratings.copy()
    numFiveStars = (true_ratings == 5).sum()
    
    oneStarIndices = numpy.where(true_ratings == 1)
    numOneStars = numpy.shape(oneStarIndices[0])[0]
    numFlippedRatings = min(numFiveStars, numOneStars)
    
    randomSubset = numpy.random.choice(numOneStars, size = numFlippedRatings, replace = False)
    flipIndices = (oneStarIndices[0][randomSubset], oneStarIndices[1][randomSubset])
    
    predictedRatings[flipIndices] = 5
    if verbose:
        print "Systems.REC_ONES: [LOG]\t", "Y.shape:", numpy.shape(true_ratings), "\t(#{5}, #{1}, #{Flip}):",\
            numFiveStars, numOneStars, numFlippedRatings
    
        print "Systems.REC_ONES: [DBG]\t sum(Y_pred - Y) = 4*#{flip} \t", (predictedRatings - true_ratings).sum()
        print "Systems.REC_ONES: [DBG]\t Rating histogram (Y|Y_pred)\t", \
            numpy.bincount(true_ratings.ravel(), minlength = 6)[1:],\
            numpy.bincount(predictedRatings.ravel(), minlength = 6)[1:]
            
    return predictedRatings

    
def REC_FOURS(true_ratings, verbose = False):
    predictedRatings = true_ratings.copy()
    numFiveStars = (true_ratings == 5).sum()
    
    fourStarIndices = numpy.where(true_ratings == 4)
    numFourStars = numpy.shape(fourStarIndices[0])[0]
    numFlippedRatings = min(numFiveStars, numFourStars)
    
    randomSubset = numpy.random.choice(numFourStars, size = numFlippedRatings, replace = False)
    flipIndices = (fourStarIndices[0][randomSubset], fourStarIndices[1][randomSubset])
    
    predictedRatings[flipIndices] = 5
    if verbose:
        print "Systems.REC_FOURS: [LOG]\t", "Y.shape:", numpy.shape(true_ratings), "\t(#{5}, #{4}, #{Flip}):",\
            numFiveStars, numFourStars, numFlippedRatings
    
        print "Systems.REC_FOURS: [DBG]\t sum(Y_pred - Y) = 4*#{flip} \t", (predictedRatings - true_ratings).sum()
        print "Systems.REC_FOURS: [DBG]\t Rating histogram (Y|Y_pred)\t", \
            numpy.bincount(true_ratings.ravel(), minlength = 6)[1:], \
            numpy.bincount(predictedRatings.ravel(), minlength = 6)[1:]
    
    return predictedRatings    

    
def ROTATE(true_ratings, verbose = False):
    predictedRatings = true_ratings.copy()
    predictedRatings = predictedRatings - 1
    predictedRatings[predictedRatings == 0] = 5
    if verbose:
        print "Systems.ROTATE: [LOG]\t", "Y.shape:", numpy.shape(true_ratings)
    
        print "Systems.ROTATE: [DBG]\t Rating histogram (Y|Y_pred)\t", \
            numpy.bincount(true_ratings.ravel(), minlength = 6)[1:], \
            numpy.bincount(predictedRatings.ravel(), minlength = 6)[1:]
    
    return predictedRatings


def COARSENED(true_ratings, verbose = False):
    predictedRatings = true_ratings.copy()
    predictedRatings[predictedRatings <= 3] = 3
    predictedRatings[predictedRatings >= 4] = 4
    if verbose:
        print "Systems.COARSENED: [LOG]\t", "Y.shape:", numpy.shape(true_ratings)
    
        print "Systems.COARSENED: [DBG]\t Rating histogram (Y|Y_pred)\t", \
            numpy.bincount(true_ratings.ravel(), minlength = 6)[1:], \
            numpy.bincount(predictedRatings.ravel(), minlength = 6)[1:]
    
    return predictedRatings


def SKEWED(true_ratings, verbose = False):
    jitter = numpy.random.standard_normal(numpy.shape(true_ratings))
    sigma = (6.0 - true_ratings).astype(numpy.float32)/2.0
    rawRatings = true_ratings + numpy.multiply(sigma, jitter)
    predictedRatings = numpy.clip(rawRatings, a_min = 0, a_max = 6)
    if verbose:
        print "Systems.SKEWED: [LOG]\t", "Y.shape:", numpy.shape(true_ratings)
    
        newHistogram = numpy.histogram(predictedRatings, bins = [0, 1.5, 2.5, 3.5, 4.5, 6])
        print "Systems.SKEWED: [DBG]\t Rating histogram (Y|Y_pred)\t", \
            numpy.bincount(true_ratings.ravel(), minlength = 6)[1:], \
            newHistogram[0]
    
    return predictedRatings
    
    
if __name__ == "__main__":
    checkY = numpy.random.random_integers(1, 5, size = (4,6))
    print "[MAIN]\t True Matrix"
    print checkY
    predictedY = REC_ONES(checkY, verbose = True)
    print "[MAIN]\t REC_ONES predicted:"
    print predictedY
    predictedY = REC_FOURS(checkY, verbose = True)
    print "[MAIN]\t REC_FOURS predicted:"
    print predictedY
    predictedY = ROTATE(checkY, verbose = True)
    print "[MAIN]\t ROTATE predicted:"
    print predictedY
    predictedY = COARSENED(checkY, verbose = True)
    print "[MAIN]\t COARSENED predicted:"
    print predictedY
    predictedY = SKEWED(checkY, verbose = True)
    print "[MAIN]\t SKEWED predicted:"
    print predictedY
    