if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import Propensity
    import pickle
    import os
    import numpy
    import itertools
    from joblib import Parallel, delayed
    import ML100KOptimization
    import MF
    import BMF

    parser = argparse.ArgumentParser(description='Recommendations on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--dataset', '-d', metavar='D', type=str, 
                        help='Dataset name', default='cs')
    parser.add_argument('--numDims', '-n', metavar='N', type=str, 
                        help='Dimension values', default='5,10,20,40')
    parser.add_argument('--numRecs', '-k', metavar='K', type=int, 
                        help='Number of recommendations', default='-1')
    parser.add_argument('--lambdas', '-l', metavar='L', type=str, 
                        help='Lambda values', default='0.008,0.04,0.2,1,5,25,125')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metrics', default='MSE')
     
    args = parser.parse_args()
    numpy.random.seed(args.seed)

    lambdas = []
    tokens = args.lambdas.strip().split(',')
    for token in tokens:
        lambdas.append(float(token))
        
    numDims = []
    tokens = args.numDims.strip().split(',')
    for token in tokens:
        numDims.append(int(token))
 
    numLambdas = len(lambdas)
    numDimSettings = len(numDims)
    biasModes = ['Regularized','Free']
    numBiasModes = len(biasModes)

    MFparamSettings = list(itertools.product(lambdas, numDims, biasModes))
    MFnumParamSettings = len(MFparamSettings)
    BMFparamSettings = list(itertools.product(numDims, biasModes))
    BMFnumParamSettings = len(BMFparamSettings)

    selfMatrix = None
    nbUnifMatrix = None
    unifMatrix = None
    lrPropensities = None
    tbsPropensities = None
    nbUnifIndices = None
    unifIndices = None
    
    if args.dataset == 'yahoo':
        selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices = Datasets.YAHOO_R3('../', 0.05)
    elif args.dataset == 'cs':
        selfMatrix, unifMatrix, lrPropensities = Datasets.COAT_SHOP('../')
        numUsers = numpy.shape(unifMatrix)[0]
        tbsPropensities = numpy.loadtxt('../data_real_users/results/lr_model/learned_propensities.ascii')
        print "Opt: [LOG] Loaded learned propensities from data_real_users", numpy.shape(tbsPropensities)
        print "Opt: [DBG]\t TBSPropensities User sum: ", \
                    numpy.sum(tbsPropensities, axis = 1, dtype = numpy.longdouble)
        unifIndices = list(range(numUsers))
    else:
        print "Unrecognized dataset ", args.dataset
        sys.exit(0)

    numUsers, numItems = numpy.shape(selfMatrix)

    ratingMarginals = None
    if nbUnifMatrix is not None:
        flatObservations = numpy.ma.compressed(nbUnifMatrix)
        ratingMarginals = numpy.bincount(flatObservations, minlength = 6)[1:]
        ratingMarginals = 1.0 * ratingMarginals / ratingMarginals.sum(dtype = numpy.longdouble)
        print "Opt: [DBG]\t Rating histogram (NB)\t", ratingMarginals

    currMetric = None
    if args.metric == 'MSE':
        currMetric = Metrics.MSE
    elif args.metric == 'MAE':
        currMetric = Metrics.MAE
    else:
        print "Opt: [ERR] Unrecognized metric for MF", args.metric
        sys.exit(0)
 
    outputFile = '../logs/csyahoo_opt/'+str(args.seed)+'_'+args.metric+'_'+args.dataset+'_'

    nbInvPropensities = None
    if nbUnifMatrix is not None:
        propensitiesPerRating = Propensity.NAIVE_BAYES_PROPENSITIES(selfMatrix, ratingMarginals)
        nbPropensities = numpy.zeros((numUsers, numItems), dtype = numpy.longdouble)
        for i in range(5):
            nbPropensities[selfMatrix == i+1] = propensitiesPerRating[i]

        nbPropensities = numpy.ma.array(nbPropensities, dtype = numpy.longdouble, copy = False, 
                                mask = nbPropensities <= 0, fill_value = 1, hard_mask = True)
        nbInvPropensities = numpy.ma.divide(1.0, nbPropensities)

    lrInvPropensities = None
    if lrPropensities is not None:
        lrInvPropensities = numpy.reciprocal(lrPropensities)
        lrInvPropensities = numpy.ma.array(lrInvPropensities, dtype = numpy.longdouble, copy = False,
                                mask = numpy.ma.getmask(selfMatrix), fill_value = 0, hard_mask = True)
    
    tbsInvPropensities = None
    if tbsPropensities is not None:
        tbsInvPropensities = numpy.reciprocal(tbsPropensities)
        tbsInvPropensities = numpy.ma.array(tbsInvPropensities, dtype = numpy.longdouble, copy = False,
                                mask = numpy.ma.getmask(selfMatrix), fill_value = 0, hard_mask = True)
   
    numRecs = args.numRecs
    ratedItemsPerUser = numpy.ma.count(selfMatrix, axis = 1)
    if args.numRecs <= 0:
        numRatings = numpy.mean(ratedItemsPerUser, dtype = numpy.longdouble)
        numRecs = int(2 * numpy.ceil(numRatings))
        if numRecs > numItems:
            numRecs = numItems
    print "Opt: [LOG] NumRecommendations: ", numRecs

    print "Opt: [DBG] Uniform Random Baseline: \t ", numpy.ma.mean(unifMatrix, dtype = numpy.longdouble)

    #TrainSetMemorizer Baseline
    unseenItems = numpy.ma.copy(unifMatrix)
    unseenItems[~numpy.ma.getmask(selfMatrix)] = numpy.ma.masked
    unseenRatings = numpy.ma.mean(unseenItems, axis = 1, dtype = numpy.longdouble)

    takenSlots = numpy.clip(ratedItemsPerUser, 0, numRecs)
    excessSlots = numRecs - takenSlots

    sortedSeenRatings = -numpy.ma.sort(-selfMatrix, axis = 1)
    for i in xrange(numUsers):
        sortedSeenRatings[i, takenSlots[i]:] = 0
    seenRatings = numpy.ma.sum(sortedSeenRatings, axis = 1, dtype = numpy.longdouble)
    totalGainPerUser = numpy.ma.multiply(unseenRatings, excessSlots) + seenRatings
    averageGain = numpy.ma.mean(totalGainPerUser / numRecs, dtype = numpy.longdouble)
    print "Opt: [DBG] TrainSet Memorizer Baseline: \t ", averageGain

    MFfoldScores = numpy.zeros((4, MFnumParamSettings), dtype = numpy.longdouble)
    WMFfoldScores = numpy.zeros((4, MFnumParamSettings), dtype = numpy.longdouble)
    BMFfoldScores = numpy.zeros((4, BMFnumParamSettings), dtype = numpy.longdouble)
       
    observationIndices = numpy.ma.nonzero(selfMatrix)
    numObservations = numpy.ma.count(selfMatrix)

    shuffleIndices = numpy.random.permutation(numObservations)
    fractionObservations = int(numObservations/4)
    firstFold = shuffleIndices[:fractionObservations]
    secondFold = shuffleIndices[fractionObservations:2*fractionObservations]
    thirdFold = shuffleIndices[2*fractionObservations:3*fractionObservations]
    fourthFold = shuffleIndices[3*fractionObservations:]
    print "Opt: [LOG] Split %d observations into folds. Fold sizes:" % len(shuffleIndices),\
                        len(firstFold), len(secondFold), len(thirdFold), len(fourthFold)
 
    for fold in xrange(4):
        print "Opt: [LOG] Fold:", fold
        trainObservations = numpy.ma.copy(selfMatrix)
        testObservations = numpy.ma.copy(selfMatrix)

        if fold == 0:
            trainObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 1:
            trainObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 2:
            trainObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 3:
            trainObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
        else:
            print "Opt: [ERR] #Folds not supported ", fold
            sys.exit(0)
        startVector = None
        bestScore = None
        # MATRIX FACTORIZATION
        for approach in ['MF', 'WMF']:
            invP = None
            if approach == 'WMF':
                if args.dataset == 'yahoo':
                    invP = nbInvPropensities
                else:
                    invP = tbsInvPropensities

            modelFileName = outputFile + 'fold' + str(fold) + '_' + approach + '.pkl'
            modelsPerLambda = None
            if os.path.exists(modelFileName):
                g = open(modelFileName, 'rb')
                modelsPerLambda = pickle.load(g)
                g.close()
                print "Opt: [LOG]\t ", approach, ": Loaded trained models for each lambda from ", modelFileName
            else:
                modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(ML100KOptimization.MF_TRAIN)(l2Lambda, 
                                                trainObservations, invP, args.metric)
                                                for l2Lambda in MFparamSettings)
                g = open(modelFileName, 'wb')
                pickle.dump(modelsPerLambda, g, -1)
                g.close()
                print "Opt: [LOG]\t ", approach, ": Saved trained models for each lambda to ", modelFileName

            for lambdaIndex, eachModel in enumerate(modelsPerLambda):
                selectedBiasMode = MFparamSettings[lambdaIndex][2]
                selectedBias = True
                if selectedBiasMode == 'None':
                    selectedBias = False
                predictedY = MF.PREDICTED_SCORES(eachModel[0], eachModel[1], 
                                                eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)
                score = None
                if invP is not None:
                    score = currMetric(testObservations, predictedY, 4.0*invP)[0]
                else:
                    score = currMetric(testObservations, predictedY, None)[0]

                if approach == 'WMF':
                    WMFfoldScores[fold, lambdaIndex] = score
                else:
                    MFfoldScores[fold, lambdaIndex] = score

                if bestScore is None or score < bestScore:
                    bestScore = score
                    startVector = eachModel

                print "Opt: [LOG] ", approach, ": Lambda/NumDims: ", MFparamSettings[lambdaIndex], \
                                "\t Test Fold Score: ", score

        # CLRS - BMF
        modelFileName = outputFile + 'fold' + str(fold) + '_BMF.pkl'
        modelsPerLambda = None
        if os.path.exists(modelFileName):
            g = open(modelFileName, 'rb')
            modelsPerLambda = pickle.load(g)
            g.close()
            print "Opt: [LOG]\t BMF: Loaded trained models for each lambda from ", modelFileName
        else:
            invP = None
            if args.dataset == 'yahoo':
                invP = nbInvPropensities
            else:
                invP = tbsInvPropensities

            modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(ML100KOptimization.BMF_TRAIN)(param, trainObservations, invP, numRecs, startVector)
                                            for param in BMFparamSettings)

            g = open(modelFileName, 'wb')
            pickle.dump(modelsPerLambda, g, -1)
            g.close()
            print "Opt: [LOG]\t BMF: Saved trained models for each lambda to ", modelFileName

        for lambdaIndex, eachModel in enumerate(modelsPerLambda):
            selectedBiasMode = BMFparamSettings[lambdaIndex][1]
            selectedBias = True
            if selectedBiasMode == 'None':
                selectedBias = False

            predictedY = BMF.PREDICTED_PROBABILITIES(eachModel[0], eachModel[1], 
                                            eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)
            score = Metrics.CG(testObservations, predictedY, 4.0*invP)[0]
            BMFfoldScores[fold, lambdaIndex] = score

            print "Opt: [LOG] BMF: NumDims: ", BMFparamSettings[lambdaIndex], \
                                "\t Test Fold Score: ", score

    startVector = None
    bestScore = None
    for approach in ['MF', 'WMF']:
        currScores = None
        if approach == 'WMF':
            currScores = WMFfoldScores
        else:
            currScores = MFfoldScores

        invP = None
        if approach == 'WMF':
            if args.dataset == 'yahoo':
                invP = nbInvPropensities
            else:
                invP = tbsInvPropensities

        allFoldScores = currScores.sum(axis = 0, dtype = numpy.longdouble)
        bestLambdaIndex = numpy.argmin(allFoldScores)
        bestLambda = MFparamSettings[bestLambdaIndex]
        print "Opt: [LOG] Retraining ", approach,": Best lambda/numDims", bestLambda
        retVal = MF.GENERATE_MATRIX(selfMatrix, invP, bestLambda[0], bestLambda[1], 'Vanilla', bias_mode = bestLambda[2], mode = args.metric, start_vec = None)
        finalBias = True
        if bestLambda[2] == 'None':
            finalBias = False
        finalPredictions = MF.PREDICTED_SCORES(retVal[0], retVal[1], retVal[2], retVal[3], retVal[4], use_bias = finalBias)
        finalPredictions = finalPredictions[unifIndices,:]
        finalPolicy = ML100KOptimization.POLICY_CREATOR(finalPredictions, numRecs)

        finalScore = Metrics.CG(unifMatrix, finalPolicy, None)[0]
        print "Opt: [DBG] Matrix Factorization Baseline: \t ", approach, 1.0 * finalScore / numRecs

        if bestScore is None or finalScore < bestScore:
            bestScore = finalScore
            startVector = retVal
 
    invP = None
    if args.dataset == 'yahoo':
        invP = nbInvPropensities
    else:
        invP = tbsInvPropensities

    allFoldScores = BMFfoldScores.sum(axis = 0, dtype = numpy.longdouble)
    bestLambdaIndex = numpy.argmax(allFoldScores)
    bestLambda = BMFparamSettings[bestLambdaIndex]
    print "Opt: [LOG] Retraining BMF: Best numDims", bestLambda

    actualStart = None
    if startVector is not None:
        numDims = bestLambda[0]
        numStartDims = numpy.shape(startVector[0])[1]

        if numDims < numStartDims:
            actualStart = (startVector[0][:,0:numDims], startVector[1][:,0:numDims],
                        startVector[2], startVector[3], startVector[4])
        else:
            startUser = numpy.zeros((numpy.shape(startVector[0])[0], numDims), dtype = numpy.longdouble)
            startItem = numpy.zeros((numpy.shape(startVector[1])[0], numDims), dtype = numpy.longdouble)

            startUser[:, 0:numStartDims] = startVector[0]
            startItem[:, 0:numStartDims] = startVector[1]
            actualStart = (startUser, startItem, startVector[2], startVector[3], startVector[4])

    retVal = BMF.LEARN(selfMatrix, invP, bestLambda[0], numRecs, bias_mode = bestLambda[1], start_vec = actualStart)
    finalBias = True
    if bestLambda[1] == 'None':
        finalBias = False
    finalPolicy = BMF.PREDICTED_PROBABILITIES(retVal[0], retVal[1], retVal[2], retVal[3], retVal[4], use_bias = finalBias)

    finalPolicy = finalPolicy[unifIndices,:]

    finalScore = Metrics.CG(unifMatrix, finalPolicy, None)[0]
    print "Opt: [DBG] Stochastic BMF: \t ", 1.0 * finalScore / numRecs

    # DIAGNOSTIC
    violations = numpy.sum(finalPolicy, axis = 1, dtype = numpy.longdouble) - numRecs
    constraintViolations = numpy.mean(numpy.abs(violations), dtype = numpy.longdouble)
    print "Opt: [DBG] Row Sums violations of Stochastic BMF: \t ", constraintViolations

    deterministicPolicy = ML100KOptimization.POLICY_CREATOR(finalPolicy, numRecs)
    finalScore = Metrics.CG(unifMatrix, deterministicPolicy, None)[0]
    print "Opt: [DBG] Deterministic BMF: \t ", 1.0 * finalScore / numRecs

