if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import pickle
    import os
    import itertools
    from joblib import Parallel, delayed
    import numpy.linalg
    import Expt2
    import Propensity
    import sys
    import MF
    import numpy
    
    parser = argparse.ArgumentParser(description='Semi-Synthetic Learning on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--trial', '-t', metavar='T', type=int, 
                        help='Trial ID', default=1)
    parser.add_argument('--bayes', '-b', metavar='N', type=str, 
                        help='Naive Bayes fraction', default='1,1e-3,2e-4,4e-5,8e-6,0')
    parser.add_argument('--lambdas', '-l', metavar='L', type=str, 
                        help='Lambda values', default='0.008,0.04,0.2,1,5,25,125')
    parser.add_argument('--numdims', '-n', metavar='N', type=str, 
                        help='Dimension values', default='20')
    parser.add_argument('--clips', '-c', metavar='C', type=str, 
                        help='Clip values', default='-1')
    parser.add_argument('--estimators', '-e', metavar='E', type=str, 
                        help='Learning methods', default='Naive,Gold-IPS,NB-IPS')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metrics', default='MSE')
                        
    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    approaches = args.estimators.strip().split(',')
    
    approachDict = {}
    for approach in approaches:
        approachDict[(approach, args.metric)] = len(approachDict)
    
    alpha = 0.25
    
    fractions = []
    tokens = args.bayes.strip().split(',')
    for token in tokens:
        fractions.append(float(token))
    
    lambdas = []
    tokens = args.lambdas.strip().split(',')
    for token in tokens:
        lambdas.append(float(token))
        
    numDims = []
    tokens = args.numdims.strip().split(',')
    for token in tokens:
        numDims.append(int(token))
    
    clipVals = []
    tokens = args.clips.strip().split(',')
    for token in tokens:
        clipVals.append(int(token))
 
    numFractions = len(fractions)
    numLambdas = len(lambdas)
    numApproaches = len(approachDict)
    biasModes = ['Free']
    numBiasModes = len(biasModes)
    
    numDimSettings = len(numDims)
    numClipSettings = len(clipVals)
    numParamSettings = numLambdas * numDimSettings * numClipSettings * numBiasModes
    
    paramSettings = list(itertools.product(lambdas, numDims, clipVals, biasModes))
    
    ML100KCompleteTest = Datasets.ML100K('../')
    
    #print "Expt2: Rank of complete test matrix:", numpy.linalg.matrix_rank(ML100KCompleteTest)
    #ratingMarginals = Datasets.RATING_MARGINALS_R3_UNIF('../')
    testHistogram = numpy.bincount(ML100KCompleteTest.ravel(), minlength = 6)[1:]
    actualRatingMarginals = 1.0 * testHistogram / testHistogram.sum()
    print "Expt5: [LOG] True Rating Marginals:", actualRatingMarginals
    
    allEstimates = numpy.zeros((numApproaches, numFractions), dtype = numpy.longdouble)
    
    currMetric = None
    if args.metric == 'MSE':
        currMetric = Metrics.MSE
    elif args.metric == 'MAE':
        currMetric = Metrics.MAE
    else:
        print "Expt5: [ERR] Unrecognized metric", args.metric
        sys.exit(0)
        
    print "Expt5: [LOG] Starting metric", args.metric
        
    def updateResults(val, approach, ind):
        approachTuple = (approach, args.metric)
        approachIndex = approachDict[approachTuple]
        allEstimates[approachIndex, ind] = val
            
    print "Expt5: [LOG] Trial", args.trial
    numpy.random.seed(args.seed + args.trial)
    
    outputFile = '../logs/expt5/'+str(args.seed)+'_'+str(args.trial)+'_'+args.metric+'_'
    
    for ind, fraction in enumerate(fractions):
        ratingMarginals = None
        if fraction == 0:
            ratingMarginals = numpy.ones(5) / 5
        else:
            nbObs, discardedProp = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, 1, fraction)
            nbHistogram = numpy.bincount(numpy.ma.compressed(nbObs), minlength = 6)[1:]
            nbHistogram = nbHistogram.astype(numpy.longdouble)
            nbHistogram += 1e-7
            ratingMarginals = nbHistogram / nbHistogram.sum(dtype = numpy.longdouble)
        print "Expt5: [LOG] Fraction:", fraction, ratingMarginals  
        sys.stdout.flush()        
        partialObservations, goldPropensities = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, alpha, 0.05)
        
        observedHistogram = numpy.bincount(numpy.ma.compressed(partialObservations), minlength = 6)[1:]
        observedHistogram = observedHistogram.astype(numpy.longdouble) / observedHistogram.sum(dtype = numpy.longdouble)
        print "Expt5: [LOG] [NB/Observed] Marginals :"
        print ratingMarginals
        print observedHistogram
        sys.stdout.flush()

        observationIndices = numpy.ma.nonzero(partialObservations)
        numObservations = numpy.ma.count(partialObservations)
        
        propensitiesPerRating = Propensity.NAIVE_BAYES_PROPENSITIES(partialObservations, ratingMarginals)
        nbPropensities = propensitiesPerRating[ML100KCompleteTest - 1]
                
        goldInvPropensities = numpy.reciprocal(goldPropensities)
        nbInvPropensities = numpy.reciprocal(nbPropensities)
        
        foldScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype = numpy.longdouble)
        foldTestScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype = numpy.longdouble)
        
        shuffleIndices = numpy.random.permutation(numObservations)
        fractionObservations = int(numObservations/4)
        firstFold = shuffleIndices[:fractionObservations]
        secondFold = shuffleIndices[fractionObservations:2*fractionObservations]
        thirdFold = shuffleIndices[2*fractionObservations:3*fractionObservations]
        fourthFold = shuffleIndices[3*fractionObservations:]
        print "Expt5: [LOG] Split %d observations into folds. Fold sizes:" % len(shuffleIndices),\
                        len(firstFold), len(secondFold), len(thirdFold), len(fourthFold)
        
        for fold in xrange(4):
            print "Expt5: [LOG] Fold:", fold
            trainObservations = numpy.ma.copy(partialObservations)
            testObservations = numpy.ma.copy(partialObservations)

            if fold == 0:
                trainObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                        numpy.ma.masked

                testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                        numpy.ma.masked
            elif fold == 1:
                trainObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                        numpy.ma.masked

                testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                        numpy.ma.masked
            elif fold == 2:
                trainObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                        numpy.ma.masked

                testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                        numpy.ma.masked
            elif fold == 3:
                trainObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                        numpy.ma.masked

                testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                        numpy.ma.masked
                testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                        numpy.ma.masked
            else:
                print "Expt2: [ERR] #Folds not supported ", fold
                sys.exit(0)
           
            #Get starting params by SVD
            startFileName = outputFile + 'fold' + str(fold) + '_' + str(fraction) +'_init.pkl'
            startTuple = None
            if os.path.exists(startFileName):
                g = open(startFileName, 'rb')
                startTuple = pickle.load(g)
                g.close()
            else:
                startTuple = Expt2.INIT_PARAMS(trainObservations, 20)
                g = open(startFileName, 'wb')
                pickle.dump(startTuple, g, -1)
                g.close()
           
            for approach in approaches:
                print "Starting approach ", approach
                invP, normN = Expt2.TRAIN_HELPER(approach, goldInvPropensities, nbInvPropensities)
                approachTuple = (approach, args.metric)
                approachIndex = approachDict[approachTuple]
                modelFileName = outputFile + 'fold' + str(fold) + '_' + str(fraction) +'_'+approach+'.pkl'
                modelsPerLambda = None
                if os.path.exists(modelFileName):
                    g = open(modelFileName, 'rb')
                    modelsPerLambda = pickle.load(g)
                    g.close()
                    print "Expt5: [LOG]\t Loaded trained models for each lambda from ", modelFileName
                else:
                    modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(Expt2.MF_TRAIN)(l2Lambda, trainObservations, 
                                            invP, normN, args.metric, startTuple)
                                            for l2Lambda in paramSettings)
                    g = open(modelFileName, 'wb')
                    pickle.dump(modelsPerLambda, g, -1)
                    g.close()
                    print "Expt5: [LOG]\t Saved trained models for each lambda to ", modelFileName
                
                for lambdaIndex, eachModel in enumerate(modelsPerLambda):
                    selectedBiasMode = paramSettings[lambdaIndex][3]
                    selectedBias = True
                    if selectedBiasMode == 'None':
                        selectedBias = False
  
                    predictedY = MF.PREDICTED_SCORES(eachModel[0], eachModel[1], eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)
                    score = None
                    if invP is not None:
                        score = currMetric(testObservations, predictedY, 4.0*invP)
                    else:
                        score = currMetric(testObservations, predictedY, invP)
                    
                    if normN == 'Vanilla':
                        score = score[0]
                    elif normN == 'SelfNormalized':
                        score = score[1]
                    elif normN == 'UserNormalized':
                        score = score[2]
                    elif normN == 'ItemNormalized':
                        score = score[3]
                    else:
                        print "Expt5: [ERR] Normalization not supported for metric ", normN, args.metric
                        sys.exit(0)
 
                    foldScores[approachIndex, fold, lambdaIndex] = score
                    
                    foldTestScore = currMetric(ML100KCompleteTest, predictedY, None)[0]
                    foldTestScores[approachIndex, fold, lambdaIndex] = foldTestScore
                    print "Expt5: [LOG] Lambda/NumDims: ", paramSettings[lambdaIndex], "\t Test Fold Score: ", score, "\t Test Set Score: ", foldTestScore
                
                #Save foldScores and foldTestScores after each approach in each fold. Overwrite if needed.
                scoresFile = outputFile + 'NBFraction'+ str(fraction)+'_foldScores.pkl'
                scoresData = (foldScores, foldTestScores)
                g = open(scoresFile, 'wb')
                pickle.dump(scoresData, g, -1)
                g.close()
                sys.stdout.flush()        
        
        eventualApproachParams = []
        for approach in approaches:
            invP, normN = Expt2.TRAIN_HELPER(approach, goldInvPropensities, nbInvPropensities)
            approachTuple = (approach, args.metric)
            approachIndex = approachDict[approachTuple]
            approachScores = foldScores[approachIndex,:,:]
            allFoldScores = approachScores.sum(axis = 0, dtype = numpy.float)
            bestLambdaIndex = numpy.argmin(allFoldScores)
            bestLambda = paramSettings[bestLambdaIndex]
            print "FINAL_TRAIN: [LOG] Retraining ",approach," Best lambda/numDims", bestLambda    
            for everyLambdaIndex, everyLambda in enumerate(paramSettings):
                print "FINAL_TRAIN: [DBG] AllFoldScores: ", approach, everyLambda, allFoldScores[everyLambdaIndex] 
            eventualApproachParams.append((approach,invP,normN,bestLambda))
            
        finalModels = None
        finalModelFileName = outputFile + str(fraction) +'_finalmodels.pkl'
        if os.path.exists(finalModelFileName):
            g = open(finalModelFileName, 'rb')
            finalModels = pickle.load(g)
            g.close()
            print "Expt5: [LOG]\t Loaded trained final models from ", finalModelFileName
        else:
            finalModels = Parallel(n_jobs = -1, verbose = 0)(delayed(Expt2.FINAL_TRAIN)(approachTup, args.metric, partialObservations, startTuple)
                                        for approachTup in eventualApproachParams)
            g = open(finalModelFileName, 'wb')
            pickle.dump(finalModels, g, -1)
            g.close()
            print "Expt5: [LOG]\t Saved trained final models to ", finalModelFileName
        
        for approachID, approachTuple in enumerate(eventualApproachParams):
            resultTuple = finalModels[approachID]
            finalBiasMode = approachTuple[3][3]
            finalBias = True
            if finalBiasMode == 'None':
                finalBias = False
            predictedY = MF.PREDICTED_SCORES(resultTuple[0],resultTuple[1],resultTuple[2],resultTuple[3],resultTuple[4], use_bias = finalBias)
            metricValue = currMetric(ML100KCompleteTest, predictedY, None)[0]
            print "Expt2: [LOG] ", approachTuple[0], "\t Eventual result:", metricValue
            sys.stdout.flush()
            updateResults(metricValue, approachTuple[0], ind)    
        
        #Dump results after each fraction. Overwrite if needed.
        outputData = (approaches, approachDict, fractions, allEstimates)
        g = open(outputFile + args.estimators +'.pkl', 'wb')
        pickle.dump(outputData, g, -1)
        g.close()
