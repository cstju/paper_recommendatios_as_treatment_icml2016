if __name__ == "__main__":
    import argparse
    import pickle
    import matplotlib
    #matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    
    parser = argparse.ArgumentParser(description='Plotting results of Expt1.py.')
    parser.add_argument('--filepath', '-f', metavar='F', type=str, 
                        help='File name of pkl', default='../logs/expt1_results_387.pkl')
    parser.add_argument('--save', dest='save', action='store_true')
    parser.add_argument('--kind', '-k', metavar='K', type=str, help='Kind of output', default='plot')
    parser.set_defaults(save=False)
    
    args = parser.parse_args()
    
    g = open(args.filepath, 'rb')
    approaches, metrics, systems, approachDict, alphas, allEstimates, allErrors = pickle.load(g)
    g.close()
    
    numTrials = numpy.shape(allEstimates)[2]
    approaches = ['Truth', 'Naive', 'Gold-IPS', 'Gold-SNIPS']
    outStr = "&".join(approaches) + "\n"
    for system in systems:
        for metric in metrics:
            def PlotErrors(approaches, output_file):
                plt.clf()
                for approach in approaches:
                    approachTup = (approach, metric, system)
                    approachInd = approachDict[approachTup]
                
                    estErrors = allErrors[approachInd, :, :]
                    perAlphaErrors = numpy.mean(estErrors, axis = 1, dtype = numpy.longdouble)
                    perAlphaStd = numpy.std(estErrors, axis = 1, dtype = numpy.longdouble)
                    perAlphaStd = 2/numpy.sqrt(numTrials)*perAlphaStd
                
                    l = plt.plot(alphas, perAlphaErrors)
                    plt.fill_between(alphas, perAlphaErrors - perAlphaStd, perAlphaErrors + perAlphaStd,
                        color=l[0].get_color(), alpha=0.2)
                    
                plt.legend(approaches, loc='best')
                plt.suptitle(system+'\t'+metric)
                plt.xlabel('Alpha')
                plt.ylabel('Estimation error')
                ax = plt.gca()
                if not numpy.any(perAlphaErrors <= 0):
                    ax.set_yscale("log")
                ax.set_xlim([0.03125, 1])
                ax.set_xscale("log")
           
                if args.save:
                    plt.savefig(outFile, format='pdf', dpi = 300)
                else:
                    plt.show()
            
            if args.kind == 'plot':
                outFile = './plot1_'+system+'_'+metric+'_ips.pdf'            
                approaches = ['Naive', 'Gold-IPS', 'Gold-SNIPS']
                PlotErrors(approaches, outFile)
                """
                outFile = './plot1_'+system+'_'+metric+'_snips.pdf'            
                approaches = ['Naive', 'Gold-SNIPS', 'NB-SNIPS']
                PlotErrors(approaches, outFile)
            
                outFile = './plot1_'+system+'_'+metric+'_unips.pdf'            
                approaches = ['Naive', 'Gold-UNIPS', 'NB-UNIPS']
                PlotErrors(approaches, outFile)
            
                outFile = './plot1_'+system+'_'+metric+'_inips.pdf'            
                approaches = ['Naive', 'Gold-INIPS', 'NB-INIPS']
                PlotErrors(approaches, outFile)
                """
            else:
                outStr += system +'_' + metric
                for approach in approaches:
                    approachTup = (approach, metric, system)
                    approachInd = approachDict[approachTup]
                
                    currEstimate = allEstimates[approachInd, :, :]
                    estimatePerAlpha = numpy.mean(currEstimate, axis = 1, dtype = numpy.longdouble)
                    perAlphaStd = numpy.std(currEstimate, axis = 1, dtype = numpy.longdouble)
                    
                    #Record values for alpha = 0.25 := ind = 2
                    outStr += '&' + ' %0.3f $\pm$ %0.3f '% (estimatePerAlpha[2], perAlphaStd[2]) + "\n"
    if args.kind != 'plot':
        outStr += '\n'
        print outStr
