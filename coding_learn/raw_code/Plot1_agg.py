if __name__ == "__main__":
    import argparse
    import pickle
    import matplotlib
    #matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    
    parser = argparse.ArgumentParser(description='Plotting results of Expt1.py.')
    parser.add_argument('--filepath', '-f', metavar='F', type=str, 
                        help='File name of pkl', default='../logs/expt1_results_387.pkl')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metric', default='MSE')

    parser.add_argument('--save', dest='save', action='store_true')
    parser.set_defaults(save=False)
    
    args = parser.parse_args()
    
    g = open(args.filepath, 'rb')
    approaches, metrics, systems, approachDict, alphas, allEstimates, allErrors = pickle.load(g)
    g.close()

    approaches = ['Naive', 'Gold-IPS', 'Gold-SNIPS']
    color_string = 'rbg'
    eventualPlot = {}
    for system in systems:
        for approach in approaches:
            approachTup = (approach, args.metric, system)
            approachInd = approachDict[approachTup]
                
            if approach not in eventualPlot:
                eventualPlot[approach] = []
            eventualPlot[approach].append(allErrors[approachInd, :, :])
               

    #titleString = 'Evaluation: Experimental setting      '+args.metric
    plt.rc('font', size = 24)
    plt.rc('text', usetex=True)
    plt.rc('font', family = 'serif')
    #plt.axes([.15,.15,.8,.8])

    legendList = []
    #plt.suptitle(titleString)
    for approachIndex, approach in enumerate(approaches):
        allTrials = numpy.hstack(eventualPlot[approach])
        allTrials = numpy.sqrt(allTrials)
        numTrials = numpy.shape(allTrials)[1]
        perAlphaErrors = numpy.mean(allTrials, axis = 1, dtype = numpy.longdouble)
        perAlphaStd = numpy.std(allTrials, axis = 1, dtype = numpy.longdouble)
        perAlphaStd = 2/numpy.sqrt(numTrials)*perAlphaStd
        lb = perAlphaErrors - perAlphaStd
        ub = perAlphaErrors + perAlphaStd
        l = plt.plot(alphas, perAlphaErrors, color=color_string[approachIndex], rasterized=True, linewidth = 2.0)
        plt.fill_between(alphas,
                     lb,
                     ub,
                     color=l[0].get_color(), alpha=0.2, rasterized=True)
        if approach == 'Gold-IPS':
            legendList.append('IPS')
        elif approach == 'Gold-SNIPS':
            legendList.append('SNIPS')
        else:
            legendList.append(approach)
                     
    plt.xlabel(r'$\boldmath{\alpha}$')
    if args.metric == 'MSE':
        plt.ylabel('Estimation error (RMSE)')
    elif args.metric == 'DCG':
        plt.legend(legendList, loc='best')

    ax = plt.gca()
    if not numpy.any(perAlphaErrors <= 0):
        ax.set_yscale("log")
    ax.set_xlim([0.03125, 1])

    plt.text(0.1, 0.1, args.metric, ha='center', va='center', transform=ax.transAxes)
    #ax.set_xscale("log")
    """
    ticks = ax.get_yticks()
    ticks = ["$10^{%d}$" % t for t in ticks]
    ax.set_yticklabels(ticks,size=20, usetex=False)
    ticks = ax.get_xticks()
    ticks = ["$10^{%d}$" % np.log10(t) for t in ticks]
    ax.set_xticklabels(ticks,size=20, usetex=False)
    """

    plt.tight_layout()

    if args.save:
        plt.savefig('./plot1_'+args.metric+'.png', format="png", dpi=100)
        plt.savefig('./plot1_'+args.metric+'.pdf', format="pdf", dpi=100)
    else:
        plt.show()
