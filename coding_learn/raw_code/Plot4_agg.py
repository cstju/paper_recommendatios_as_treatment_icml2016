if __name__ == "__main__":
    import argparse
    import pickle
    import matplotlib
    #matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    
    parser = argparse.ArgumentParser(description='Plotting results of Expt4.py.')
    parser.add_argument('--filepath', '-f', metavar='F', type=str, 
                        help='File name of pkl', default='../logs/expt4_results_387.pkl')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metric', default='MSE')
    parser.add_argument('--save', dest='save', action='store_true')
    parser.set_defaults(save=False)
    
    args = parser.parse_args()
    
    g = open(args.filepath, 'rb')
    approaches, metrics, systems, approachDict, fractions, allEstimates, allErrors = pickle.load(g)
    g.close()
   
    fractions = numpy.array(fractions) * 1586126

    numTrials = numpy.shape(allEstimates)[2]
    approaches = ['Naive','Gold-IPS','NB-IPS','NB-SNIPS']
    color_string = 'rbgm'
    eventualPlot = {}
    for system in systems:
        for approach in approaches:
            approachTup = (approach, args.metric, system)
            approachInd = approachDict[approachTup]
            
            if approach not in eventualPlot:
                eventualPlot[approach] = []
            eventualPlot[approach].append(allErrors[approachInd, :, :])

    #titleString = 'Evaluation: Observational setting      '+args.metric
    plt.rc('font', size = 24)
    plt.rc('text', usetex=True)
    plt.rc('font', family = 'serif')
    #plt.axes([.2,.2,.8,.8])

    legendList = []
    #plt.suptitle(titleString)
    for approachIndex, approach in enumerate(approaches):  
        allTrials = numpy.hstack(eventualPlot[approach])
        allTrials = numpy.sqrt(allTrials)
        numTrials = numpy.shape(allTrials)[1]
        perAlphaErrors = numpy.mean(allTrials, axis = 1, dtype = numpy.longdouble)
        perAlphaStd = numpy.std(allTrials, axis = 1, dtype = numpy.longdouble)
        perAlphaStd = 2/numpy.sqrt(numTrials)*perAlphaStd
        
        l = plt.plot(fractions, perAlphaErrors, color=color_string[approachIndex], rasterized=True, linewidth = 2.0)
        plt.fill_between(fractions, perAlphaErrors - perAlphaStd, perAlphaErrors + perAlphaStd,
                        color=l[0].get_color(), alpha=0.2, rasterized = True)
        if approach == 'Gold-IPS':
            legendList.append('IPS')
        elif approach == 'NB-IPS':
            legendList.append('IPS-NB')
        elif approach == 'NB-SNIPS':
            legendList.append('SNIPS-NB')
        else:
            legendList.append(approach)
                    
    plt.xlabel(r'Number of MCAR ratings seen by NB')
    if args.metric == 'MSE':
        plt.ylabel('Estimation error (RMSE)')
    elif args.metric == 'DCG':
        plt.legend(legendList, loc='best')

    ax = plt.gca()
    if not numpy.any(perAlphaErrors <= 0):
        ax.set_yscale("log")
    ax.set_xlim([0, 1586126])
    ax.set_xscale("symlog", linthreshx = fractions[-2])
          
    plt.text(0.1, 0.1, args.metric, ha='center', va='center', transform=ax.transAxes) 
    plt.tight_layout()

    if args.save:
        plt.savefig('./plot4_'+args.metric+'.png', format="png", dpi=100)
        plt.savefig('./plot4_'+args.metric+'.pdf', format="pdf", dpi=100)
    else:
        plt.show()
