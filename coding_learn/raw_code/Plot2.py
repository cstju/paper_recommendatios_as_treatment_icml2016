import re 
import pylab 

if __name__ == "__main__":
    import argparse
    import pickle
    import matplotlib
    #matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy
    import glob
    import matplotlib.ticker as ticker
    
    parser = argparse.ArgumentParser(description='Plotting results of Expt2.py.')
    parser.add_argument('--filepath', '-f', metavar='F', type=str, 
                        help='File name of pkl', default='../logs/expt2/387_*_MSE_*,*.pkl')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metric', default='MSE')
    parser.add_argument('--save', dest='save', action='store_true')
    parser.set_defaults(save=False)
    
    args = parser.parse_args()
    files = glob.glob(args.filepath)

    trials = []
    
    approaches = ['Naive','Gold-IPS', 'Gold-SNIPS']
    color_string = 'rbg'

    #titleString = 'Learning: Experimental setting      '+args.metric
    plt.rc('font', size = 24)
    plt.rc('text', usetex=True)
    plt.rc('font', family = 'serif')
    #plt.axes([.1,.1,.8,.8])
    #plt.suptitle(titleString)

    approachDict = {}
    for approach in approaches:
        approachDict[(approach, args.metric)] = len(approachDict)
        
    alphas = [1,0.5,0.25,0.125,0.0625,0.03125]
    numAlphas = len(alphas)
    numApproaches = len(approachDict)

    
    results = {}
    for fileName in files:
        startIndex = fileName.index('387_') + 4
        endIndex = fileName.index('_', startIndex)
        trialID = int(fileName[startIndex:endIndex])
        if trialID not in results:
            results[trialID] = numpy.zeros((numApproaches, numAlphas), dtype = numpy.float)
            
        g = open(fileName, 'rb')
        currentApproaches, currentApproachDict, currentAlphas, currentEstimates = pickle.load(g)
        g.close()
        for eachApproach in currentApproaches:
            tupleKey = (eachApproach, args.metric)
            currentIndex = currentApproachDict[tupleKey]
            globalIndex = approachDict[tupleKey]
            
            results[trialID][globalIndex,:] = currentEstimates[currentIndex,:]
            
    allEstimates = numpy.dstack(results.values())
    numTrials = numpy.shape(allEstimates)[2]
    
    def PlotErrors(nowApproaches, output_file):
        #plt.clf()
        legendList = []
        for approachIndex,approach in enumerate(nowApproaches):
            if approach == 'Gold-SNIPS':
                continue
            approachTup = (approach, args.metric)
            approachInd = approachDict[approachTup]
                
            estimates = allEstimates[approachInd, :, :]
            perAlphaEstimates = numpy.mean(estimates, axis = 1, dtype = numpy.longdouble)
            perAlphaStd = numpy.std(estimates, axis = 1, dtype = numpy.longdouble)
            perAlphaStd = 2/numpy.sqrt(numTrials)*perAlphaStd
            l = plt.plot(alphas, perAlphaEstimates, color=color_string[approachIndex], rasterized=True, linewidth = 2.0)
            plt.fill_between(alphas, perAlphaEstimates - perAlphaStd, perAlphaEstimates + perAlphaStd,
                        color=l[0].get_color(), alpha=0.2, rasterized=True)

            if approach == 'Gold-IPS':
                legendList.append('MF-IPS')
            elif approach == 'Gold-SNIPS':
                legendList.append('MF-SNIPS')
            else:
                legendList.append('MF-Naive')
            
        plt.legend(legendList, loc='best')
        #plt.suptitle(args.metric)
        plt.xlabel(r'$\boldmath{\alpha}$')
        plt.ylabel(args.metric)
        ax = plt.gca()

        ax.set_yscale("log")            
        #plt.tick_params(axis='y', which='minor')
        #ax.yaxis.set_minor_formatter(FormatStrFormatter())
        ax.yaxis.set_minor_locator( ticker.MultipleLocator(base = 0.25) )
        ax.yaxis.set_major_formatter( ticker.ScalarFormatter() )
        ax.yaxis.set_minor_formatter( ticker.ScalarFormatter() )

        ax.set_xlim([0.03125, 1])
        ax.set_ylim([0, 1.95])
        #ax.set_xscale("log")

        plt.tight_layout()

        if args.save:
            plt.savefig(outFile+'.png', format='png', dpi = 100)
            plt.savefig(outFile+'.pdf', format='pdf', dpi = 100)
        else:
            plt.show()
            
    outFile = './plot2_'+args.metric
    approaches = ['Naive', 'Gold-IPS', 'Gold-SNIPS']
    PlotErrors(approaches, outFile)
    """        
    outFile = './plot2_'+args.metric+'_snips.pdf'            
    approaches = ['Naive', 'Gold-SNIPS', 'NB-SNIPS']
    PlotErrors(approaches, outFile)
            
    outFile = './plot2_'+args.metric+'_unips.pdf'            
    approaches = ['Naive', 'Gold-UNIPS', 'NB-UNIPS']
    PlotErrors(approaches, outFile)
            
    outFile = './plot2_'+args.metric+'_inips.pdf'            
    approaches = ['Naive', 'Gold-INIPS', 'NB-INIPS']
    PlotErrors(approaches, outFile)        
    """
