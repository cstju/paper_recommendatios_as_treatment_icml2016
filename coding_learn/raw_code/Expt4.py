if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import Propensity
    import Systems
    import itertools
    import sys
    import numpy
    import pickle
    
    parser = argparse.ArgumentParser(description='Semi-Synthetic Evaluation on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--trials', '-t', metavar='T', type=int, 
                        help='Trials for each alpha', default=50)
    parser.add_argument('--nb', '-n', metavar='N', type=str, 
                        help='Naive Bayes fraction', default='1,1e-3,2e-4,4e-5,8e-6,0')

    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    approaches = ['Truth','Naive','Gold-IPS','NB-IPS','Gold-SNIPS','NB-SNIPS']
    metrics = ['MSE','MAE','DCG']
    systems = ['COARSENED','REC_ONES','REC_FOURS','ROTATE','SKEWED']
    
    approachDict = {}
    for tup in itertools.product(approaches, metrics, systems):
        approachDict[tup] = len(approachDict)
    
    alpha = 0.25
    
    fractions = []
    tokens = args.nb.strip().split(',')
    for token in tokens:
        fractions.append(float(token))
    
    numFractions = len(fractions)
    numApproaches = len(approachDict)
    
    ML100KCompleteTest = Datasets.ML100K('../')
    numUsers, numItems = numpy.shape(ML100KCompleteTest)
    Metrics.dcgJitter = Metrics.Jitter(50, numUsers, numItems)
    
    testHistogram = numpy.bincount(ML100KCompleteTest.ravel(), minlength = 6)[1:]
    actualRatingMarginals = 1.0 * testHistogram / testHistogram.sum()
    print "Expt4: [LOG] True Rating Marginals:", actualRatingMarginals
                
    allEstimates = numpy.zeros((numApproaches, numFractions, args.trials), dtype = numpy.longdouble)
    allErrors = numpy.zeros((numApproaches, numFractions, args.trials), dtype = numpy.longdouble)
    
    for system in systems:
        sysCall = None
        if system == 'REC_ONES':
            sysCall = Systems.REC_ONES
        elif system == 'REC_FOURS':
            sysCall = Systems.REC_FOURS
        elif system == 'ROTATE':
            sysCall = Systems.ROTATE
        elif system == 'COARSENED':
            sysCall = Systems.COARSENED
        elif system == 'SKEWED':
            sysCall = Systems.SKEWED
        else:
            print "Expt4: [ERR] Unrecognized system", system
            sys.exit(0)
        print "Expt4: [LOG] Starting system", system            
        predictedY = sysCall(ML100KCompleteTest)
               
        for metric in metrics:
            currMetric = None
            if metric == 'MSE':
                currMetric = Metrics.MSE
            elif metric == 'MAE':
                currMetric = Metrics.MAE
            elif metric == 'DCG':
                currMetric = Metrics.DCG
            else:
                print "Expt4: [ERR] Unrecognized metric", metric
                sys.exit(0)
        
            print "Expt4: [LOG] Starting metric", metric
            truth = currMetric(ML100KCompleteTest, predictedY, None)[0]
            approachTuple = ('Truth',metric,system)
            approachIndex = approachDict[approachTuple]
            
            allEstimates[approachIndex, :, :] = truth
 
            print "Truth", truth
            sys.stdout.flush()        
            def updateResults(tup, approachList, ind, trial):
                for i, approach in enumerate(approachList):
                    approachTuple = (approach, metric, system)
                    approachIndex = approachDict[approachTuple]
                    allEstimates[approachIndex, ind, trial] = tup[i]
                    allErrors[approachIndex, ind, trial] = (tup[i] - truth)**2
            
            
            for ind, fraction in enumerate(fractions):
                ratingMarginals = None
                if fraction == 0:
                    ratingMarginals = numpy.ones(5, dtype = numpy.longdouble) / 5.0
                else:
                    nbObs, discardedProp = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, 1, fraction)
                    flatObservations = numpy.ma.compressed(nbObs)
                    nbHistogram = numpy.bincount(flatObservations, minlength = 6)[1:]
                    nbHistogram = nbHistogram.astype(numpy.longdouble)
                    nbHistogram += 1
                    ratingMarginals = nbHistogram / nbHistogram.sum(dtype = numpy.longdouble)

                print "Expt4: [LOG] Fraction:", fraction, ratingMarginals  
                
                for trial in xrange(args.trials):
                    observations, goldPropensities = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, alpha, 0.05)
                    propensitiesPerRating = Propensity.NAIVE_BAYES_PROPENSITIES(observations, ratingMarginals)
                    nbPropensities = propensitiesPerRating[ML100KCompleteTest - 1]

                    goldInvPropensities = numpy.reciprocal(goldPropensities)
                    nbInvPropensities = numpy.reciprocal(nbPropensities)
                    
                    metricValues = \
                                currMetric(observations, predictedY, goldInvPropensities)
                    updateResults(metricValues, ['Gold-IPS', 'Gold-SNIPS'], ind, trial)
                    #print "Gold", metricValues
                    
                    metricValues = \
                                currMetric(observations, predictedY, None)
                    updateResults(metricValues, ['Naive'], ind, trial)
                    #print "Naive", metricValues
                    
                    metricValues = \
                                currMetric(observations, predictedY, nbInvPropensities)
                    updateResults(metricValues, ['NB-IPS', 'NB-SNIPS'], ind, trial)
                    #print "NB", metricValues
                    
                    if trial % 10 == 0:
                        print ".",
                        sys.stdout.flush()
                        
                print ""
            
            #Save pkl after each metric for each system, overwrite previous pkl
            outputFile = '../logs/expt4_results_'+str(args.seed)+'.pkl'
            outputData = (approaches, metrics, systems, approachDict, fractions, allEstimates, allErrors)
            g = open(outputFile, 'wb')
            pickle.dump(outputData, g, -1)
            g.close()
