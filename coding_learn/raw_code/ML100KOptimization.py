import MF
import BMF
import numpy
 

def MF_TRAIN(params, train_observations, inv_propensities, metric):
    retVal = None
    tempInvPropensities = None
    if inv_propensities is not None:
        tempInvPropensities = (4.0 / 3.0) * inv_propensities

    retVal = MF.GENERATE_MATRIX(train_observations, tempInvPropensities, params[0], params[1], 'Vanilla', bias_mode = params[2], mode = metric, start_vec = None)
    return retVal


def BMF_TRAIN(params, train_observations, inv_propensities, num_recommendations, start_vector):
    retVal = None
    tempInvPropensities = None
    if inv_propensities is not None:
        tempInvPropensities = (4.0 / 3.0) * inv_propensities

    actualStart = None
    """
    if start_vector is not None:
        numDims = params[0]
        numStartDims = numpy.shape(start_vector[0])[1]

        if numDims < numStartDims:
            actualStart = (start_vector[0][:,0:numDims], start_vector[1][:,0:numDims],
                        start_vector[2], start_vector[3], start_vector[4])
        else:
            startUser = numpy.zeros((numpy.shape(start_vector[0])[0], numDims), dtype = numpy.longdouble)
            startItem = numpy.zeros((numpy.shape(start_vector[1])[0], numDims), dtype = numpy.longdouble)

            startUser[:, 0:numStartDims] = start_vector[0]
            startItem[:, 0:numStartDims] = start_vector[1]
            actualStart = (startUser, startItem, start_vector[2], start_vector[3], start_vector[4])
    """
    retVal = BMF.LEARN(train_observations, tempInvPropensities, params[0], params[1], num_recommendations, params[2], start_vec = actualStart)
    return retVal


def POLICY_CREATOR(predicted_scores, num_recommendations):
    numUsers, numItems = numpy.shape(predicted_scores)
    numRecs = num_recommendations
    if numItems < num_recommendations:
        numRecs = numItems
        print "POLICY_CREATOR: [LOG] Re-setting NumRecommendations: ", num_recommendations, "to NumItems: ", numItems

    policy = numpy.zeros((numUsers, numItems), dtype = numpy.longdouble)
    sortedScores = numpy.ma.argsort(-predicted_scores, axis = 1)
    topScores = sortedScores[:, 0:numRecs]
   
    staticIndices = numpy.ogrid[0:numUsers, 0:numItems]
    policy[staticIndices[0], topScores] = 1.0
    return policy   

 
if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import Propensity
    import pickle
    import os
    import itertools
    from joblib import Parallel, delayed

    parser = argparse.ArgumentParser(description='Recommendations on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--trial', '-t', metavar='T', type=int, 
                        help='Trial ID', default=1)
    parser.add_argument('--numDims', '-d', metavar='D', type=str, 
                        help='Dimension values', default='2,4,8,16,32')
    parser.add_argument('--numRecs', '-k', metavar='K', type=int, 
                        help='Number of recommendations', default='-1')
    parser.add_argument('--alpha', '-a', metavar='A', type=float, 
                        help='Alpha value for observation model', default='0.25')
    parser.add_argument('--lambdas', '-l', metavar='L', type=str, 
                        help='Lambda values', default='0.008,0.04,0.2,1,5,25,125')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metrics', default='MSE')
     
    args = parser.parse_args()
    numpy.random.seed(args.seed)

    lambdas = []
    tokens = args.lambdas.strip().split(',')
    for token in tokens:
        lambdas.append(float(token))
        
    numDims = []
    tokens = args.numDims.strip().split(',')
    for token in tokens:
        numDims.append(int(token))
 
    numLambdas = len(lambdas)
    numDimSettings = len(numDims)
    biasModes = ['Free']
    numBiasModes = len(biasModes)

    MFparamSettings = list(itertools.product(lambdas, numDims, biasModes))
    MFnumParamSettings = len(MFparamSettings)

    ML100KCompleteTest = Datasets.ML100K('../')
    numUsers, numItems = numpy.shape(ML100KCompleteTest)

    subsetUsers = numpy.random.choice(numUsers, size = int(numUsers / 20), replace = False)
    subsetItems = numpy.random.choice(numItems, size = int(numItems / 20), replace = False)

    ML100KCompleteTest = ML100KCompleteTest[subsetUsers,:]
    ML100KCompleteTest = ML100KCompleteTest[:, subsetItems]

    numUsers, numItems = numpy.shape(ML100KCompleteTest)

    currMetric = None
    if args.metric == 'MSE':
        currMetric = Metrics.MSE
    elif args.metric == 'MAE':
        currMetric = Metrics.MAE
    else:
        print "Opt: [ERR] Unrecognized metric for MF", args.metric
        sys.exit(0)
 
    print "Opt: [LOG] Trial", args.trial
    numpy.random.seed(args.seed + args.trial)
    
    outputFile = '../logs/ml100k_opt/'+str(args.seed)+'_'+str(args.trial)+'_'+args.metric+'_'
 
    partialObservations, goldPropensities = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, args.alpha, 0.05)

    flatObservations = numpy.ma.compressed(partialObservations) 
    observedHistogram = numpy.bincount(flatObservations, minlength = 6)[1:]
    observedHistogram = observedHistogram.astype(numpy.longdouble) / \
                            observedHistogram.sum(dtype = numpy.longdouble)

    print "Opt: [LOG] Observed Marginals: ", observedHistogram
    
    numRecs = args.numRecs
    ratedItemsPerUser = numpy.ma.count(partialObservations, axis = 1)
    if args.numRecs <= 0:
        numRatings = numpy.mean(ratedItemsPerUser, dtype = numpy.longdouble)
        numRecs = int(2 * numpy.ceil(numRatings))
        if numRecs > numItems:
            numRecs = numItems
    print "Opt: [LOG] NumRecommendations: ", numRecs

    print "Opt: [DBG] Uniform Random Baseline: \t ", numpy.mean(ML100KCompleteTest, dtype = numpy.longdouble)

    #TrainSetMemorizer Baseline
    unseenItems = numpy.ma.array(ML100KCompleteTest, dtype = numpy.int, copy = True, 
                        mask = ~numpy.ma.getmask(partialObservations), fill_value = 0, hard_mask = True)
    unseenRatings = numpy.ma.mean(unseenItems, axis = 1, dtype = numpy.longdouble)

    takenSlots = numpy.clip(ratedItemsPerUser, 0, numRecs)
    excessSlots = numRecs - takenSlots

    sortedSeenRatings = -numpy.ma.sort(-partialObservations, axis = 1)
    for i in xrange(numUsers):
        sortedSeenRatings[i, takenSlots[i]:] = 0
    seenRatings = numpy.ma.sum(sortedSeenRatings, axis = 1, dtype = numpy.longdouble)
    totalGainPerUser = numpy.ma.multiply(unseenRatings, excessSlots) + seenRatings
    averageGain = numpy.ma.mean(totalGainPerUser / numRecs, dtype = numpy.longdouble)
    print "Opt: [DBG] TrainSet Memorizer Baseline: \t ", averageGain

    bestPolicy = POLICY_CREATOR(ML100KCompleteTest, numRecs)
    bestGain = Metrics.CG(ML100KCompleteTest, bestPolicy, None)
    print "Opt: [DBG] Skyline: \t ", 1.0 * bestGain[0] / numRecs

    MFfoldScores = numpy.zeros((4, MFnumParamSettings), dtype = numpy.longdouble)
    WMFfoldScores = numpy.zeros((4, MFnumParamSettings), dtype = numpy.longdouble)
    BMFfoldScores = numpy.zeros((4, MFnumParamSettings), dtype = numpy.longdouble)
       
    goldInvPropensities = numpy.reciprocal(goldPropensities)

    observationIndices = numpy.ma.nonzero(partialObservations)
    numObservations = numpy.ma.count(partialObservations)

    shuffleIndices = numpy.random.permutation(numObservations)
    fractionObservations = int(numObservations/4)
    firstFold = shuffleIndices[:fractionObservations]
    secondFold = shuffleIndices[fractionObservations:2*fractionObservations]
    thirdFold = shuffleIndices[2*fractionObservations:3*fractionObservations]
    fourthFold = shuffleIndices[3*fractionObservations:]
    print "Opt: [LOG] Split %d observations into folds. Fold sizes:" % len(shuffleIndices),\
                        len(firstFold), len(secondFold), len(thirdFold), len(fourthFold)
 
    for fold in xrange(4):
        print "Opt: [LOG] Fold:", fold
        trainObservations = numpy.ma.copy(partialObservations)
        testObservations = numpy.ma.copy(partialObservations)

        if fold == 0:
            trainObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 1:
            trainObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 2:
            trainObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 3:
            trainObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
        else:
            print "Opt: [ERR] #Folds not supported ", fold
            sys.exit(0)

        startVector = None
        bestScore = None
        # MATRIX FACTORIZATION
        for approach in ['MF', 'WMF']:
            invP = None
            if approach == 'WMF':
                invP = goldInvPropensities

            modelFileName = outputFile + 'fold' + str(fold) + '_' + approach + '.pkl'
            modelsPerLambda = None
            if os.path.exists(modelFileName):
                g = open(modelFileName, 'rb')
                modelsPerLambda = pickle.load(g)
                g.close()
                print "Opt: [LOG]\t ", approach, ": Loaded trained models for each lambda from ", modelFileName
            else:
                modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(MF_TRAIN)(l2Lambda, trainObservations, 
                                                invP, args.metric)
                                                for l2Lambda in MFparamSettings)
                g = open(modelFileName, 'wb')
                pickle.dump(modelsPerLambda, g, -1)
                g.close()
                print "Opt: [LOG]\t ", approach, ": Saved trained models for each lambda to ", modelFileName

            for lambdaIndex, eachModel in enumerate(modelsPerLambda):
                selectedBiasMode = MFparamSettings[lambdaIndex][2]
                selectedBias = True
                if selectedBiasMode == 'None':
                    selectedBias = False

                predictedY = MF.PREDICTED_SCORES(eachModel[0], eachModel[1], 
                                                eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)
                score = None
                if invP is not None:
                    score = currMetric(testObservations, predictedY, 4.0*invP)[0]
                else:
                    score = currMetric(testObservations, predictedY, None)[0]

                if approach == 'WMF':
                    WMFfoldScores[fold, lambdaIndex] = score
                else:
                    MFfoldScores[fold, lambdaIndex] = score

                if bestScore is None or score < bestScore:
                    bestScore = score
                    startVector = eachModel

                print "Opt: [LOG] ", approach, ": Lambda/NumDims: ", MFparamSettings[lambdaIndex], \
                                "\t Test Fold Score: ", score

        # CLRS - BMF
        modelFileName = outputFile + 'fold' + str(fold) + '_BMF.pkl'
        modelsPerLambda = None
        if os.path.exists(modelFileName):
            g = open(modelFileName, 'rb')
            modelsPerLambda = pickle.load(g)
            g.close()
            print "Opt: [LOG]\t BMF: Loaded trained models for each lambda from ", modelFileName
        else:
            modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(BMF_TRAIN)(param, trainObservations, goldInvPropensities, numRecs, startVector)
                                            for param in MFparamSettings)

            g = open(modelFileName, 'wb')
            pickle.dump(modelsPerLambda, g, -1)
            g.close()
            print "Opt: [LOG]\t BMF: Saved trained models for each lambda to ", modelFileName

        for lambdaIndex, eachModel in enumerate(modelsPerLambda):
            selectedBiasMode = MFparamSettings[lambdaIndex][2]
            selectedBias = True
            if selectedBiasMode == 'None':
                selectedBias = False

            predictedY = BMF.PREDICTED_PROBABILITIES(eachModel[0], eachModel[1], 
                                            eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)

            score = Metrics.CG(testObservations, predictedY, 4.0*goldInvPropensities)[0]
            BMFfoldScores[fold, lambdaIndex] = score

            print "Opt: [LOG] BMF: NumDims: ", MFparamSettings[lambdaIndex], \
                                "\t Test Fold Score: ", score

    startVector = None
    bestScore = None
    for approach in ['MF', 'WMF']:
        currScores = None
        if approach == 'WMF':
            currScores = WMFfoldScores
        else:
            currScores = MFfoldScores

        invP = None
        if approach == 'WMF':
            invP = goldInvPropensities

        allFoldScores = currScores.sum(axis = 0, dtype = numpy.longdouble)
        bestLambdaIndex = numpy.argmin(allFoldScores)
        bestLambda = MFparamSettings[bestLambdaIndex]
        print "Opt: [LOG] Retraining ", approach,": Best lambda/numDims", bestLambda
        retVal = MF.GENERATE_MATRIX(partialObservations, invP, bestLambda[0], bestLambda[1], 'Vanilla', bias_mode = bestLambda[2], mode = args.metric, start_vec = None)
        finalBias = True
        if bestLambda[2] == 'None':
            finalBias = False
        finalPredictions = MF.PREDICTED_SCORES(retVal[0], retVal[1], retVal[2], retVal[3], retVal[4], use_bias = finalBias)
        finalPolicy = POLICY_CREATOR(finalPredictions, numRecs)

        finalScore = Metrics.CG(ML100KCompleteTest, finalPolicy, None)[0]
        print "Opt: [DBG] Matrix Factorization Baseline: \t ", approach, 1.0 * finalScore / numRecs

        if bestScore is None or finalScore < bestScore:
            bestScore = finalScore
            startVector = retVal
   
    allFoldScores = BMFfoldScores.sum(axis = 0, dtype = numpy.longdouble)
    bestLambdaIndex = numpy.argmax(allFoldScores)
    bestLambda = MFparamSettings[bestLambdaIndex]
    print "Opt: [LOG] Retraining BMF: Best numDims", bestLambda

    actualStart = None
    """
    if startVector is not None:
        numDims = bestLambda[0]
        numStartDims = numpy.shape(startVector[0])[1]

        if numDims < numStartDims:
            actualStart = (startVector[0][:,0:numDims], startVector[1][:,0:numDims],
                        startVector[2], startVector[3], startVector[4])
        else:
            startUser = numpy.zeros((numpy.shape(startVector[0])[0], numDims), dtype = numpy.longdouble)
            startItem = numpy.zeros((numpy.shape(startVector[1])[0], numDims), dtype = numpy.longdouble)

            startUser[:, 0:numStartDims] = startVector[0]
            startItem[:, 0:numStartDims] = startVector[1]
            actualStart = (startUser, startItem, startVector[2], startVector[3], startVector[4])
    """
    retVal = BMF.LEARN(partialObservations, goldInvPropensities, bestLambda[0], bestLambda[1], numRecs, bias_mode = bestLambda[2], start_vec = actualStart)
    finalBias = True
    if bestLambda[2] == 'None':
        finalBias = False
    finalPolicy = BMF.PREDICTED_PROBABILITIES(retVal[0], retVal[1], retVal[2], retVal[3], retVal[4], use_bias = finalBias)

    finalScore = Metrics.CG(ML100KCompleteTest, finalPolicy, None)[0]
    print "Opt: [DBG] Stochastic BMF: \t ", 1.0 * finalScore / numRecs

    # DIAGNOSTIC
    violations = numpy.sum(finalPolicy, axis = 1, dtype = numpy.longdouble) - numRecs
    constraintViolations = numpy.mean(numpy.abs(violations), dtype = numpy.longdouble)
    print "Opt: [DBG] Row Sums violations of Stochastic BMF: \t ", constraintViolations

    deterministicPolicy = POLICY_CREATOR(finalPolicy, numRecs)
    finalScore = Metrics.CG(ML100KCompleteTest, deterministicPolicy, None)[0]
    print "Opt: [DBG] Deterministic BMF: \t ", 1.0 * finalScore / numRecs

