if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import Propensity
    import Systems
    import itertools
    import sys
    import numpy
    import pickle
    
    parser = argparse.ArgumentParser(description='Semi-Synthetic Evaluation on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--trials', '-t', metavar='T', type=int, 
                        help='Trials for each alpha', default=50)
    parser.add_argument('--alphas', '-a', metavar='A', type=str, 
                        help='Alpha values', default='1,0.5,0.25,0.125,0.0625,0.03125')

    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    approaches = ['Truth','Naive','Gold-IPS','Gold-SNIPS']
    metrics = ['MSE','MAE','DCG']
    systems = ['COARSENED','REC_ONES','REC_FOURS','ROTATE','SKEWED']
    
    approachDict = {}
    for tup in itertools.product(approaches, metrics, systems):
        approachDict[tup] = len(approachDict)
        
    alphas = []
    tokens = args.alphas.strip().split(',')
    for token in tokens:
        alphas.append(float(token))
    
    numAlphas = len(alphas)
    numApproaches = len(approachDict)
    
    ML100KCompleteTest = Datasets.ML100K('../')
    numUsers, numItems = numpy.shape(ML100KCompleteTest)
    ratingMarginals = Datasets.RATING_MARGINALS_R3_UNIF('../')
    Metrics.dcgJitter = Metrics.Jitter(50, numUsers, numItems)
    
    allEstimates = numpy.zeros((numApproaches, numAlphas, args.trials), dtype = numpy.longdouble)
    allErrors = numpy.zeros((numApproaches, numAlphas, args.trials), dtype = numpy.longdouble)
    
    for system in systems:
        sysCall = None
        if system == 'REC_ONES':
            sysCall = Systems.REC_ONES
        elif system == 'REC_FOURS':
            sysCall = Systems.REC_FOURS
        elif system == 'ROTATE':
            sysCall = Systems.ROTATE
        elif system == 'COARSENED':
            sysCall = Systems.COARSENED
        elif system == 'SKEWED':
            sysCall = Systems.SKEWED
        else:
            print "Expt1: [ERR] Unrecognized system", system
            sys.exit(0)
        print "Expt1: [LOG] Starting system", system            
        predictedY = sysCall(ML100KCompleteTest)
               
        for metric in metrics:
            currMetric = None
            if metric == 'MSE':
                currMetric = Metrics.MSE
            elif metric == 'MAE':
                currMetric = Metrics.MAE
            elif metric == 'DCG':
                currMetric = Metrics.DCG
            else:
                print "Expt1: [ERR] Unrecognized metric", metric
                sys.exit(0)
        
            print "Expt1: [LOG] Starting metric", metric
            truth = currMetric(ML100KCompleteTest, predictedY, None)[0]
            approachTuple = ('Truth',metric,system)
            approachIndex = approachDict[approachTuple]
            
            allEstimates[approachIndex, :, :] = truth
 
            print "Truth", truth
            sys.stdout.flush()        
            def updateResults(tup, approachList, ind, trial):
                for i, approach in enumerate(approachList):
                    approachTuple = (approach, metric, system)
                    approachIndex = approachDict[approachTuple]
                    allEstimates[approachIndex, ind, trial] = tup[i]
                    allErrors[approachIndex, ind, trial] = (tup[i] - truth)**2
            
            
            for ind, alpha in enumerate(alphas):
                print "Expt1: [LOG] Alpha:", alpha
                for trial in xrange(args.trials):
                    observations, goldPropensities = Propensity.PARTIAL_OBSERVE(ML100KCompleteTest, alpha, 0.05)
                
                    goldInvPropensities = numpy.reciprocal(goldPropensities)
                    
                    metricValues = \
                                currMetric(observations, predictedY, goldInvPropensities)
                    updateResults(metricValues, ['Gold-IPS', 'Gold-SNIPS'], ind, trial)
                    #print "Gold", metricValues
                    
                    metricValues = \
                                currMetric(observations, predictedY, None)
                    updateResults(metricValues, ['Naive'], ind, trial)
                    #print "Naive", metricValues
                    
                    if trial % 10 == 0:
                        print ".",
                        sys.stdout.flush()
                        
                print ""
            
            #Save pkl after each metric for each system, overwrite previous pkl
            outputFile = '../logs/expt1_results_'+str(args.seed)+'.pkl'
            outputData = (approaches, metrics, systems, approachDict, alphas, allEstimates, allErrors)
            g = open(outputFile, 'wb')
            pickle.dump(outputData, g, -1)
            g.close()
