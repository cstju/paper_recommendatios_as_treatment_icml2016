def TRAIN_HELPER(approach, nb_inv_propensities, lr_inv_propensities, tbs_inv_propensities):
    invP = None
    if approach == 'Naive':
        invP = None
    elif approach.startswith('NB'):
        invP = nb_inv_propensities
    elif approach.startswith('LR'):
        invP = lr_inv_propensities
    elif approach.startswith('TBS'):
        invP = tbs_inv_propensities
    else:
        print "TRAIN_HELPER: [ERR] Unrecognized approach", approach
        sys.exit(0)
                        
    normN = None
    if approach == 'Naive' or approach.endswith('-IPS'):
        normN = 'Vanilla'
    elif approach.endswith('-SNIPS'):
        normN = 'SelfNormalized'
    elif approach.endswith('-UNIPS'):
        normN = 'UserNormalized'
    elif approach.endswith('-INIPS'):
        normN = 'ItemNormalized'
    else:
        print "TRAIN_HELPER: [ERR] Unrecognized approach", approach
        sys.exit(0)
        
    return invP, normN    


if __name__ == "__main__":
    import argparse
    import Datasets
    import Metrics
    import Propensity
    import sys
    import numpy
    import pickle
    import Expt2
    import os
    import MF
    import itertools
    from joblib import Parallel, delayed
    
    parser = argparse.ArgumentParser(description='Real Learning on ML100K.')
    parser.add_argument('--seed', '-s', metavar='S', type=int, 
                        help='Seed for numpy.random', default=387)
    parser.add_argument('--dataset', '-d', metavar='D', type=str, 
                        help='Dataset name', default='yahoo')
    parser.add_argument('--lambdas', '-l', metavar='L', type=str, 
                        help='Lambda values', default='0.008,0.04,0.2,1,5,25,125')
    parser.add_argument('--numdims', '-n', metavar='N', type=str, 
                        help='Dimension values', default='5,10,20,40')
    parser.add_argument('--clips', '-c', metavar='C', type=str, 
                        help='Clip values', default='-1')
    parser.add_argument('--estimators', '-e', metavar='E', type=str, 
                        help='Learning methods', default='Naive,NB-IPS')
    parser.add_argument('--metric', '-m', metavar='M', type=str, 
                        help='Metrics', default='MSE')
    
    args = parser.parse_args()
    numpy.random.seed(args.seed)
    
    approaches = args.estimators.strip().split(',')
    
    approachDict = {}
    for approach in approaches:
        approachDict[(approach, args.metric)] = len(approachDict)
        
    lambdas = []
    tokens = args.lambdas.strip().split(',')
    for token in tokens:
        lambdas.append(float(token))
        
    numDims = []
    tokens = args.numdims.strip().split(',')
    for token in tokens:
        numDims.append(int(token))
 
    clipVals = []
    tokens = args.clips.strip().split(',')
    for token in tokens:
        clipVals.append(int(token))
   
    biasModes = ['Free','Regularized']
    numBiasModes = len(biasModes)
    numLambdas = len(lambdas)
    numDimSettings = len(numDims)
    numClipSettings = len(clipVals)
    numParamSettings = numLambdas * numDimSettings * numClipSettings * numBiasModes
    
    paramSettings = list(itertools.product(lambdas, numDims, clipVals, biasModes))
    
    numApproaches = len(approachDict)
    
    selfMatrix = None
    nbUnifMatrix = None
    unifMatrix = None
    lrPropensities = None
    tbsPropensities = None
    nbUnifIndices = None
    unifIndices = None
    
    if args.dataset == 'yahoo':
        selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices = Datasets.YAHOO_R3('../', 0.05)
    elif args.dataset == 'cs':
        selfMatrix, unifMatrix, lrPropensities = Datasets.COAT_SHOP('../')
        tbsPropensities = numpy.loadtxt('../data_real_users/results/lr_model/learned_propensities.ascii')
        print "Expt3: [LOG] Loaded learned propensities from data_real_users", numpy.shape(tbsPropensities)
        print "Expt3: [DBG]\t TBSPropensities User sum: ", numpy.sum(tbsPropensities, axis = 1, dtype = numpy.longdouble)
        numUsers = numpy.shape(unifMatrix)[0]
        unifIndices = list(range(numUsers))
    else:
        print "Unrecognized dataset ", args.dataset
        sys.exit(0)

    numUnifUsers = numpy.shape(unifMatrix)[0]
    
    ratingMarginals = None
    if nbUnifMatrix is not None:
        flatObservations = numpy.ma.compressed(nbUnifMatrix)
        ratingMarginals = numpy.bincount(flatObservations, minlength = 6)[1:]
        ratingMarginals = 1.0 * ratingMarginals / ratingMarginals.sum(dtype = numpy.longdouble)
        print "Expt3: [DBG]\t Rating histogram (NB)\t", ratingMarginals
    
    allEstimates = numpy.zeros(numApproaches, dtype = numpy.longdouble)
    currMetric = None
    if args.metric == 'MSE':
        currMetric = Metrics.MSE
    elif args.metric == 'MAE':
        currMetric = Metrics.MAE
    else:
        print "Expt3: [ERR] Unrecognized metric", args.metric
        sys.exit(0)
    
    print "Expt3: [LOG] Starting metric", args.metric
            
    def updateResults(val, approach):
        approachTuple = (approach, args.metric)
        approachIndex = approachDict[approachTuple]
        allEstimates[approachIndex] = val
            
    outputFile = '../logs/expt3/'+str(args.seed)+'_'+args.metric+'_'+args.dataset+'_' 
    
    nbInvPropensities = None
    if nbUnifMatrix is not None:
        propensitiesPerRating = Propensity.NAIVE_BAYES_PROPENSITIES(selfMatrix, ratingMarginals)
        numUsers, numItems = numpy.shape(selfMatrix)
        nbPropensities =  numpy.zeros((numUsers, numItems), dtype = numpy.longdouble)
        for i in range(5):
            nbPropensities[selfMatrix == i+1] = propensitiesPerRating[i]
        nbPropensities = numpy.ma.array(nbPropensities, dtype = numpy.longdouble, copy = False,
                mask = nbPropensities <= 0, fill_value = 1, hard_mask = True)

        nbInvPropensities = numpy.ma.divide(1.0, nbPropensities)

    lrInvPropensities = None
    if lrPropensities is not None:
        lrInvPropensities = numpy.reciprocal(lrPropensities)
        lrInvPropensities = numpy.ma.array(lrInvPropensities, dtype = numpy.longdouble, copy = False,
                mask = numpy.ma.getmask(selfMatrix), fill_value = 0, hard_mask = True)
    
    tbsInvPropensities = None
    if tbsPropensities is not None:
        tbsInvPropensities = numpy.reciprocal(tbsPropensities)
        tbsInvPropensities = numpy.ma.array(tbsInvPropensities, dtype = numpy.longdouble, copy = False,
                mask = numpy.ma.getmask(selfMatrix), fill_value = 0, hard_mask = True)
 
        
    foldScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype = numpy.float)
    foldTestScores = numpy.zeros((numApproaches, 4, numParamSettings), dtype = numpy.float)
    
    observationIndices = numpy.ma.nonzero(selfMatrix)
    numObservations = numpy.ma.count(selfMatrix)
        
    shuffleIndices = numpy.random.permutation(numObservations)
    fractionObservations = int(numObservations/4)
    firstFold = shuffleIndices[:fractionObservations]
    secondFold = shuffleIndices[fractionObservations:2*fractionObservations]
    thirdFold = shuffleIndices[2*fractionObservations:3*fractionObservations]
    fourthFold = shuffleIndices[3*fractionObservations:]
    print "Expt3: [LOG] Split %d observations into folds. Fold sizes:" % len(shuffleIndices),\
                    len(firstFold), len(secondFold), len(thirdFold), len(fourthFold)    
        
    for fold in xrange(4):
        print "Expt3: [LOG] Fold:", fold
        trainObservations = numpy.ma.copy(selfMatrix)
        testObservations = numpy.ma.copy(selfMatrix)

        if fold == 0:
            trainObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 1:
            trainObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 2:
            trainObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked
        elif fold == 3:
            trainObservations[observationIndices[0][fourthFold], observationIndices[1][fourthFold]] = \
                                    numpy.ma.masked

            testObservations[observationIndices[0][firstFold], observationIndices[1][firstFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][secondFold], observationIndices[1][secondFold]] = \
                                    numpy.ma.masked
            testObservations[observationIndices[0][thirdFold], observationIndices[1][thirdFold]] = \
                                    numpy.ma.masked
        else:
            print "Opt: [ERR] #Folds not supported ", fold
            sys.exit(0)
       
        #Get starting params by SVD
        startFileName = outputFile + 'fold' + str(fold) + '_init.pkl'
        startTuple = None
        if os.path.exists(startFileName):
            g = open(startFileName, 'rb')
            startTuple = pickle.load(g)
            g.close()
        else:
            startTuple = Expt2.INIT_PARAMS(trainObservations, 40)
            g = open(startFileName, 'wb')
            pickle.dump(startTuple, g, -1)
            g.close()
                       
        for approach in approaches:
            print "Starting approach ", approach
            invP, normN = TRAIN_HELPER(approach, nbInvPropensities, lrInvPropensities, tbsInvPropensities)
            approachTuple = (approach, args.metric)
            approachIndex = approachDict[approachTuple]
            modelFileName = outputFile + 'fold' + str(fold) + '_'+approach+'.pkl'
            modelsPerLambda = None
            if os.path.exists(modelFileName):
                g = open(modelFileName, 'rb')
                modelsPerLambda = pickle.load(g)
                g.close()
                print "Expt3: [LOG]\t Loaded trained models for each lambda from ", modelFileName
            else:
                modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(Expt2.MF_TRAIN)(param,
                trainObservations, invP, normN, args.metric, startTuple)
                                            for param in paramSettings)
                g = open(modelFileName, 'wb')
                pickle.dump(modelsPerLambda, g, -1)
                g.close()
                print "Expt3: [LOG]\t Saved trained models for each lambda to ", modelFileName 
                
            for lambdaIndex, eachModel in enumerate(modelsPerLambda):
                selectedBiasMode = paramSettings[lambdaIndex][3]
                selectedBias = True
                if selectedBiasMode == 'None':
                    selectedBias = False
                predictedY = MF.PREDICTED_SCORES(eachModel[0], eachModel[1],
                                eachModel[2], eachModel[3], eachModel[4], use_bias = selectedBias)

                score = None
                if invP is not None:
                    score = currMetric(testObservations, predictedY, 4.0*invP)
                else:
                    score = currMetric(testObservations, predictedY, invP)
                        
                if normN == 'Vanilla':
                    score = score[0]
                elif normN == 'SelfNormalized':
                    score = score[1]
                elif normN == 'UserNormalized':
                    score = score[2]
                elif normN == 'ItemNormalized':
                    score = score[3]
                else:
                    print "Expt3: [ERR] Normalization not supported for metric ", normN, args.metric
                    sys.exit(0)

                foldScores[approachIndex, fold, lambdaIndex] = score
                
                unifUserY = predictedY[unifIndices,:]
                
                foldTestScore = currMetric(unifMatrix, unifUserY, None)[0]
                foldTestScores[approachIndex, fold, lambdaIndex] = foldTestScore
                print "Expt3: [LOG] Lambda/NumDims: ", paramSettings[lambdaIndex],\
                        "\t Test Fold Score: ", score, "\t Test Set Score: ", foldTestScore

            #Save foldScores and foldTestScores after each approach in each fold.
            scoresFile = outputFile + 'foldScores.pkl'
            scoresData = (foldScores, foldTestScores)
            g = open(scoresFile, 'wb')
            pickle.dump(scoresData, g, -1)
            g.close()
            sys.stdout.flush() 
        
    eventualApproachParams = []
    for approach in approaches:
        invP, normN = TRAIN_HELPER(approach, nbInvPropensities, lrInvPropensities, tbsInvPropensities)
        approachTuple = (approach, args.metric)
        approachIndex = approachDict[approachTuple]
        approachScores = foldScores[approachIndex,:,:]
        allFoldScores = approachScores.sum(axis = 0, dtype = numpy.float)
        bestLambdaIndex = numpy.argmin(allFoldScores)
        bestLambda = paramSettings[bestLambdaIndex]
        print "FINAL_TRAIN: [LOG] Retraining ",approach," Best lambda/NumDims:", bestLambda    
        for everyLambdaIndex, everyLambda in enumerate(paramSettings):
            print "FINAL_TRAIN: [DBG] AllFoldScores: ", approach, everyLambda, allFoldScores[everyLambdaIndex]
        eventualApproachParams.append((approach,invP,normN,bestLambda))
    
    finalModels = None
    finalModelFileName = outputFile + '_finalmodels.pkl'
    if os.path.exists(finalModelFileName):
        g = open(finalModelFileName, 'rb')
        finalModels = pickle.load(g)
        g.close()
        print "Expt3: [LOG]\t Loaded trained final models from ", finalModelFileName
    else:
        finalModels = Parallel(n_jobs = -1, verbose = 0)(delayed(Expt2.FINAL_TRAIN)(approachTup, 
                                        args.metric, selfMatrix, startTuple)
                                        for approachTup in eventualApproachParams)
        g = open(finalModelFileName, 'wb')
        pickle.dump(finalModels, g, -1)
        g.close()
        print "Expt3: [LOG]\t Saved trained final models to ", finalModelFileName
    
    for approachID, approachTuple in enumerate(eventualApproachParams):
        resultTuple = finalModels[approachID]
        finalBiasMode = approachTuple[3][3]
        finalBias = True
        if finalBiasMode == 'None':
            finalBias = False

        predictedY = MF.PREDICTED_SCORES(resultTuple[0],resultTuple[1],
                                    resultTuple[2],resultTuple[3],resultTuple[4], use_bias = finalBias)
                                    
        unifUserY = predictedY[unifIndices, :]
        metricValue = currMetric(unifMatrix, unifUserY, None)[0]
        print "Expt3: [LOG] ", approachTuple[0], "\t Eventual result:", metricValue
        sys.stdout.flush()
        updateResults(metricValue, approachTuple[0])  
            
    outputData = (approaches, approachDict, allEstimates)
    g = open(outputFile+ args.estimators +'.pkl', 'wb')
    pickle.dump(outputData, g, -1)
    g.close()
    
