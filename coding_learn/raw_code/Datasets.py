import numpy
import scipy.sparse
import MF
import Propensity
import os
import pickle
from joblib import Parallel, delayed
import itertools
import sys


def RATING_MARGINALS_R3_UNIF(repo_path, verbose = False):
    g = open(repo_path+"/evaluation/exp/data/ydata-test.txt", 'r')
    ratingMarginals = numpy.zeros(5, dtype = numpy.longdouble)
    for line in g:
        tokens = line.strip().split('\t')
        val = int(tokens[2])
        ratingMarginals[val - 1] += 1
    g.close()
    if verbose:
        print "Datasets.RATING_MARGINALS_R3_UNIF: [LOG]\t Repo path", repo_path, "Rating counts", ratingMarginals
    ratingMarginals = ratingMarginals / ratingMarginals.sum(dtype = numpy.longdouble)
    if verbose:
        print "Datasets.RATING_MARGINALS_R3_UNIF: [DBG]\t Ratings distribution", ratingMarginals
    return ratingMarginals
    

def COAT_SHOP(repo_path, verbose = False):
    if verbose:
        print "Datasets.COAT_SHOP: [LOG]\t Repository path:", repo_path

    csPklFile = repo_path+'/logs/cs.pkl'
        
    selfMatrix = None
    unifMatrix = None
    lrPropensities = None
    if os.path.exists(csPklFile):
        g = open(csPklFile, 'rb')
        selfMatrix, unifMatrix, lrPropensities = pickle.load(g)
        g.close()
        print "Datasets.COAT_SHOP: [DBG]\t Loaded datasets from ", csPklFile, "\t Shape: ",\
                numpy.shape(selfMatrix), numpy.shape(unifMatrix), numpy.shape(lrPropensities)
    else:
        f = open(repo_path+"/data_real_users/results/rating_self_selected.sparse.ascii", 'r')
        rows = []
        cols = []
        vals = []
        numSelfUsers = 0
        numItems = 0
        for line in f:
            tokens = line.strip().split(' ')
            rowIndex = int(tokens[0])
            colIndex = int(tokens[1])
            rows.append(rowIndex)
            cols.append(colIndex)
            vals.append(int(tokens[2]))
        
            if rowIndex >= numSelfUsers:
                numSelfUsers = rowIndex + 1
            if colIndex >= numItems:
                numItems = colIndex + 1
            
        f.close()
    
        numSelfRatings = len(vals)
        if verbose:
            print "Datasets.COAT_SHOP: [DBG]\t NumSelfUsers,NumItems,NumSelfObservations:", \
                numSelfUsers, numItems, numSelfRatings
    
        selfMatrix = scipy.sparse.coo_matrix((vals, (rows,cols)), shape = (numSelfUsers, numItems),
                        dtype = numpy.int)
        selfMatrix = selfMatrix.toarray()
 
        selfMatrix = numpy.ma.array(selfMatrix, dtype = numpy.int, copy = False, 
                            mask = selfMatrix <= 0, fill_value = 0, hard_mask = True)
        
        f = open(repo_path+"/data_real_users/results/rating_randomly_selected.sparse.ascii", 'r')
        rows = []
        cols = []
        vals = []
        numUnifUsers = 0
        for line in f:
            tokens = line.strip().split(' ')
            rowIndex = int(tokens[0])
            colIndex = int(tokens[1])
            rows.append(rowIndex)
            cols.append(colIndex)
            vals.append(int(tokens[2]))
            if rowIndex >= numUnifUsers:
                numUnifUsers = rowIndex + 1
        f.close()
    
        numUnifRatings = len(vals)
        rows = numpy.array(rows)
        cols = numpy.array(cols)
        vals = numpy.array(vals)
        if verbose:
            print "Datasets.COAT_SHOP: [DBG]\t NumUnifUsers,NumItems,NumUnifObservations:",\
                    numUnifUsers, numItems, numUnifRatings
            
        unifMatrix = scipy.sparse.coo_matrix((vals, (rows,cols)), shape = (numUnifUsers, numItems),
                        dtype = numpy.int)
        unifMatrix = unifMatrix.toarray()

        unifMatrix = numpy.ma.array(unifMatrix, dtype = numpy.int, copy = False, 
                            mask = unifMatrix <= 0, fill_value = 0, hard_mask = True)
        
        overlappingObservations = numpy.logical_and(~numpy.ma.getmaskarray(selfMatrix), 
                                                    ~numpy.ma.getmaskarray(unifMatrix))
        numOverlap = overlappingObservations.sum(dtype = numpy.int)
        print "Overlapping observations: Count/Fraction", numOverlap, numOverlap*1.0/numUnifRatings
        
        userFeatures = None
        itemFeatures = None
        observations = None
        featurePklFile = repo_path+'/logs/cs_features.pkl'
        if os.path.exists(featurePklFile):
            g = open(featurePklFile, 'rb')
            userFeatures, itemFeatures, observations = pickle.load(g)
            g.close()
            print "Datasets.COAT_SHOP: [DBG]\t Loaded user/item features from ",\
                        featurePklFile, "\t Shape: ", numpy.shape(userFeatures), numpy.shape(itemFeatures)
        else:
            f = open(repo_path+"/data_real_users/results/items.ascii", 'rb')
            genderList = ['men','women']
            jacketList = ['bomber','cropped','field','fleece','insulated','motorcycle',
                    'other','packable','parkas','pea','rain','shells','track','trench','vests','waterproof']
            colorList = ['beige','black','blue','brown','gray','green','multi','navy','olive',
                    'other','pink','purple','red']
            frontpageList = ['yes','no']
            
            numItemFeatures = len(genderList) + len(jacketList) + len(colorList) + len(frontpageList)
            
            itemFeatures = numpy.zeros((numItems, numItemFeatures), dtype = numpy.int)
            header = False
            itemID = -1
            for line in f:
                if not header:
                    header = True
                    continue
                tokens = line.strip().split('\t')
                itemID += 1
                offset = 0
                genderType = tokens[1].lower()
                genderID = genderList.index(genderType)
                itemFeatures[itemID, genderID+offset] = 1
                offset += len(genderList)
                
                jacketType = tokens[2].lower()
                jacketID = jacketList.index(jacketType)
                itemFeatures[itemID, jacketID+offset] = 1
                offset += len(jacketList)
                
                colorType = tokens[3].lower()
                colorID = colorList.index(colorType)
                itemFeatures[itemID, colorID+offset] = 1
                offset += len(colorList)
                
                fpType = tokens[4].lower()
                fpID = frontpageList.index(fpType)
                itemFeatures[itemID, fpID+offset] = 1
            f.close()    
                
            f = open(repo_path+"/data_real_users/results/users.ascii", 'rb')
            genderList = ['male','female']
            ageList = ['20-30','30-40','40-50','50-60','over 60','under 20']
            locationList = ['rural','suburban','urban']
            fashionList = ['moderately','not at all','very']
            
            numUserFeatures = len(genderList) + len(ageList) + len(locationList) + len(fashionList)
            userFeatures = numpy.zeros((numSelfUsers, numUserFeatures), dtype = numpy.int)
            header = False
            userID = -1
            for line in f:
                if not header:
                    header = True
                    continue
                tokens = line.strip().split('\t')
                userID += 1
                offset = 0
                genderType = tokens[1].lower()
                genderID = genderList.index(genderType)
                userFeatures[userID, genderID+offset] = 1
                offset += len(genderList)
            
                ageType = tokens[2].lower()
                ageID = ageList.index(ageType)
                userFeatures[userID, ageID+offset] = 1
                offset += len(ageList)
            
                locationType = tokens[3].lower()
                locationID = locationList.index(locationType)
                userFeatures[userID, locationID+offset] = 1
                offset += len(locationList)
            
                fashionType = tokens[4].lower()
                fashionID = fashionList.index(fashionType)
                userFeatures[userID, fashionID+offset] = 1
        
            f = open(repo_path+"/data_real_users/results/observations.sparse.ascii", 'r')
            rows = []
            cols = []
            vals = []
            for line in f:
                tokens = line.strip().split(' ')
                rowIndex = int(tokens[0])
                colIndex = int(tokens[1])
                rows.append(rowIndex)
                cols.append(colIndex)
                vals.append(int(tokens[2]))
    
            f.close()
            numObservations = len(vals)
            rows = numpy.array(rows)
            cols = numpy.array(cols)
            vals = numpy.array(vals)
    
            observations = scipy.sparse.coo_matrix((vals, (rows,cols)), shape = (numSelfUsers, numItems),
                    dtype = numpy.int)
            observations = observations.toarray()
    
            featuresData = (userFeatures, itemFeatures, observations)
            f = open(featurePklFile, 'wb')
            pickle.dump(featuresData, f, -1)
            f.close()
            print "Datasets.COAT_SHOP: [DBG]\t Saved user/item features to ",\
                    featurePklFile, "\t Shape: ", numpy.shape(userFeatures), numpy.shape(itemFeatures),\
                    numpy.shape(observations)
            
        lrPropensities = Propensity.CS_LR_PROPENSITIES(userFeatures, itemFeatures, observations, verbose)
            
        outputData = (selfMatrix, unifMatrix, lrPropensities)
        g = open(csPklFile, 'wb')
        pickle.dump(outputData, g, -1)
        g.close()
        
    return selfMatrix, unifMatrix, lrPropensities    


def YAHOO_R3(repo_path, nb_fraction, verbose = False):
    if verbose:
        print "Datasets.YAHOO_R3: [LOG]\t Repository path:", repo_path
    
    yahooPklFile = repo_path+'/logs/yahooR3_'+str(nb_fraction)+'.pkl'
    selfMatrix = None
    nbUnifMatrix = None
    unifMatrix = None
    nbUnifIndices = None
    unifIndices = None
    if os.path.exists(yahooPklFile):
        g = open(yahooPklFile, 'rb')
        selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices = pickle.load(g)
        g.close()
        print "Datasets.YAHOO_R3: [DBG]\t Loaded datasets from ", yahooPklFile, "\t Shape: ",\
                numpy.shape(selfMatrix), numpy.shape(unifMatrix), len(unifIndices)
    else:
        f = open(repo_path+"/evaluation/exp/data/ydata-train.txt", 'r')
        rows = []
        cols = []
        vals = []
        numSelfUsers = 0
        numItems = 0
        for line in f:
            tokens = line.strip().split('\t')
            rowIndex = int(tokens[0])-1
            colIndex = int(tokens[1])-1
            rows.append(rowIndex)
            cols.append(colIndex)
            vals.append(int(tokens[2]))
        
            if rowIndex >= numSelfUsers:
                numSelfUsers = rowIndex + 1
            if colIndex >= numItems:
                numItems = colIndex + 1
            
        f.close()
    
        numSelfRatings = len(vals)
        if verbose:
            print "Datasets.YAHOO_R3: [DBG]\t NumSelfUsers,NumItems,NumSelfObservations:", \
                numSelfUsers, numItems, numSelfRatings
    
        selfMatrix = scipy.sparse.coo_matrix((vals, (rows,cols)), shape = (numSelfUsers, numItems),
                        dtype = numpy.int)
        selfMatrix = selfMatrix.toarray()

        selfMatrix = numpy.ma.array(selfMatrix, dtype = numpy.int, copy = False, 
                            mask = selfMatrix <= 0, fill_value = 0, hard_mask = True)
 
        f = open(repo_path+"/evaluation/exp/data/ydata-test.txt", 'r')
        rows = []
        cols = []
        vals = []
        numUnifUsers = 0
        for line in f:
            tokens = line.strip().split('\t')
            rowIndex = int(tokens[0])-1
            colIndex = int(tokens[1])-1
            rows.append(rowIndex)
            cols.append(colIndex)
            vals.append(int(tokens[2]))
            if rowIndex >= numUnifUsers:
                numUnifUsers = rowIndex + 1
        f.close()
    
        numUnifRatings = len(vals)
        rows = numpy.array(rows)
        cols = numpy.array(cols)
        vals = numpy.array(vals)
        if verbose:
            print "Datasets.YAHOO_R3: [DBG]\t NumUnifUsers,NumItems,NumUnifObservations:", \
                numUnifUsers, numItems, numUnifRatings

        allUnifMatrix = scipy.sparse.coo_matrix((vals, (rows,cols)), shape = (numUnifUsers, numItems),
                        dtype = numpy.int)
        allUnifMatrix = allUnifMatrix.toarray()

        allUnifMatrix = numpy.ma.array(allUnifMatrix, dtype = numpy.int, copy = False, 
                            mask = allUnifMatrix <= 0, fill_value = 0, hard_mask = True)
 
        if nb_fraction > 0:
            shuffleOrder = numpy.random.permutation(numUnifUsers)
            nbUserFraction = int(nb_fraction * numUnifUsers)
    
            nbUnifIndices = shuffleOrder[0:nbUserFraction]
            nbUnifMatrix = allUnifMatrix[nbUnifIndices,:]
    
            unifIndices = shuffleOrder[nbUserFraction:]
            unifMatrix = allUnifMatrix[unifIndices,:]
    
            if verbose:
                print "Datasets.YAHOO_R3: [DBG]\t NB/Rest Unif shape: ", numpy.shape(nbUnifMatrix),\
                        numpy.shape(unifMatrix)
        else:
            nbUnifMatrix = None
            unifMatrix = allUnifMatrix
            unifIndices = range(numUnifUsers)
            nbUnifIndices = None
 
           
        outputData = (selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices)
        g = open(yahooPklFile, 'wb')
        pickle.dump(outputData, g, -1)
        g.close()
    
    return selfMatrix, nbUnifMatrix, unifMatrix, nbUnifIndices, unifIndices
   

def MF_TRAINHELPER(train_matrix, inverse_propensities, l2_lambda, num_dims, cumulative_rating, test_matrix):
    userVectors, itemVectors, userBiases, itemBiases, globalBias = MF.GENERATE_MATRIX(train_matrix, 
                        inverse_propensities, l2_lambda, num_dims, normalization = 'Vanilla', 
                        bias_mode = 'None', mode = 'MSE', start_vec = None, verbose = False)
    completeScores = MF.PREDICTED_SCORES(userVectors, itemVectors, userBiases, itemBiases, globalBias, False)

    currentTestMatrix = numpy.empty(numpy.shape(train_matrix), dtype = numpy.int)
    sortedScores = numpy.sort(completeScores, axis = None)
    numEntries = numpy.shape(sortedScores)[0]
    checkPoints = [sortedScores[0]]
    for i in range(5):
        ind = int(cumulative_rating[i] * numEntries)
        checkPoints.append(sortedScores[ind-1])

    for i in range(1,6):
        mask = numpy.logical_and(completeScores >= checkPoints[i-1], completeScores <= checkPoints[i])
        currentTestMatrix[mask] = i

    #Now check the quality of this completion
    matches = currentTestMatrix == test_matrix
    numMatches = numpy.ma.sum(matches, dtype = numpy.int)

    hist, binEdges = numpy.histogram(numpy.clip(completeScores, 1, 5), bins = range(1,7), density = False)
    hist = hist.astype(numpy.longdouble) / hist.sum(dtype = numpy.longdouble)
    print "Datasets.MF_TRAINHELPER: [DBG]\t Lambda:", l2_lambda, "NumDims:", num_dims, \
            "\t NumMatches:", numMatches," \t Histogram:", hist, binEdges
    sys.stdout.flush()

    return (numMatches, currentTestMatrix)

            
def ML100K(repo_path, verbose = False):
    if verbose:
        print "Datasets.ML100K: [LOG]\t Repository path:", repo_path
 
    completeTestFile = repo_path+'/logs/ml100k_10_complete_ratings.pkl'
    completeTestMatrix = None
    if os.path.exists(completeTestFile):
        g = open(completeTestFile, 'rb')
        completeTestMatrix = pickle.load(g)
        g.close()
        print "Datasets.ML100K: [DBG]\t Loaded complete test matrix from ", completeTestFile, "\t Shape: ",\
                numpy.shape(completeTestMatrix)
    else:
        f = open(repo_path+"/evaluation/exp/data/u.data", 'r')
        rows = []
        cols = []
        vals = []
        numUsers = 0
        numItems = 0
        for line in f:
            tokens = line.strip().split('\t')
            rowIndex = int(tokens[0])-1
            colIndex = int(tokens[1])-1
            rows.append(rowIndex)
            cols.append(colIndex)
            vals.append(int(tokens[2]))
        
            if rowIndex >= numUsers:
                numUsers = rowIndex + 1
            if colIndex >= numItems:
                numItems = colIndex + 1
            
        f.close()
    
        numRatings = len(vals)
        rows = numpy.array(rows)
        cols = numpy.array(cols)
        vals = numpy.array(vals)
    
        if verbose:
            print "Datasets.ML100K: [LOG]\t NumUsers,NumItems,NumObservations:", numUsers, numItems, numRatings
    
        shuffleOrder = numpy.random.permutation(numRatings)
        testFraction = int(0.1 * numRatings)
    
        testIndices = shuffleOrder[0:testFraction]
        testRows = rows[testIndices]
        testCols = cols[testIndices]
        testVals = vals[testIndices]
        testMatrix = scipy.sparse.coo_matrix((testVals, (testRows,testCols)), shape = (numUsers, numItems),
                        dtype = numpy.int)
        testMatrix = testMatrix.toarray()
    
        testMatrix = numpy.ma.array(testMatrix, dtype = numpy.int, copy = False, 
                            mask = testMatrix <= 0, fill_value = 0, hard_mask = True)

        #Finish loading the training set ratings
        trainIndices = shuffleOrder[testFraction:]
        trainRows = rows[trainIndices]
        trainCols = cols[trainIndices]
        trainVals = vals[trainIndices]
        trainMatrix = scipy.sparse.coo_matrix((trainVals, (trainRows,trainCols)), shape = (numUsers, numItems),
                        dtype = numpy.int)
        trainMatrix = trainMatrix.toarray()
    
        trainMatrix = numpy.ma.array(trainMatrix, dtype = numpy.int, copy = False, 
                            mask = trainMatrix <= 0, fill_value = 0, hard_mask = True)

        ratingMarginals = RATING_MARGINALS_R3_UNIF(repo_path)
        ratingCumulative = numpy.cumsum(ratingMarginals, dtype = numpy.longdouble)

        propensityPerRating = Propensity.NAIVE_BAYES_PROPENSITIES(trainMatrix, ratingMarginals, verbose = True)
        allPropensities = numpy.zeros(numpy.shape(trainMatrix), dtype = numpy.longdouble)
        for ind in range(1,6):
            allPropensities[trainMatrix == ind] = propensityPerRating[ind-1]
            allPropensities[testMatrix == ind] = propensityPerRating[ind-1]
        
        allPropensities = numpy.ma.array(allPropensities, dtype = numpy.longdouble, copy = False, 
                            mask = allPropensities <= 0, fill_value = 1, hard_mask = True)

        invPropensities = numpy.ma.divide(1.0, allPropensities)

        lambdas = [1,5,25,125]
        dims = [200]
        
        paramSettings = list(itertools.product(lambdas, dims))
    
        modelsPerLambda = Parallel(n_jobs = -1, verbose = 0)(delayed(MF_TRAINHELPER)(trainMatrix, invPropensities,
                                                    l2Lambda[0], l2Lambda[1], ratingCumulative, testMatrix)
                                                    for l2Lambda in paramSettings)
                                                                    
        bestMatchScore = None
        completeTestMatrix = None
        for ind, eachModel in enumerate(modelsPerLambda):            
            print "Datasets.ML100K: [DBG]\t Parameters:", paramSettings[ind], "\t Matches:", eachModel[0]
            if (bestMatchScore is None) or (eachModel[0] > bestMatchScore):
                bestMatchScore = eachModel[0]
                completeTestMatrix = eachModel[1]
                
        print "Datasets.ML100K: [DBG]\t Best matching completion \t Matches:", bestMatchScore
        
        flatObservations = numpy.ma.compressed(testMatrix)
        empiricalCounts = numpy.bincount(flatObservations, minlength = 6)[1:]
        print "Datasets.ML100K: [DBG]\t Frequency of test ratings \t ", empiricalCounts
        
        g = open(completeTestFile, 'wb')
        pickle.dump(completeTestMatrix, g, -1)
        g.close()
        print "Datasets.ML100K: [DBG]\t Saved complete test matrix to ", completeTestFile, "\t Shape: ",\
                numpy.shape(completeTestMatrix)

    ratingRange = completeTestMatrix.max() - completeTestMatrix.min() + 1
    print "Datasets.ML100K: [DBG]\t Histogram of complete test ratings", \
                numpy.bincount(completeTestMatrix.ravel(), minlength = ratingRange + 1)[1:]
   
    return completeTestMatrix
   
    
if __name__ == "__main__":
    ratingMarginals = RATING_MARGINALS_R3_UNIF('../', verbose = True)
    COAT_SHOP('../', verbose = True)
    YAHOO_R3('../', 0.05, verbose = True)

    numpy.random.seed(387)
    test = ML100K('../', verbose = True)

    #Also test Propensity.py methods here, since we have ML100K data already
    testObservations, propensities = Propensity.PARTIAL_OBSERVE(test, 0.25, 0.05, verbose = True)
    estimatedPropensities = Propensity.NAIVE_BAYES_PROPENSITIES(testObservations, ratingMarginals, verbose = True)
    print "Naive Bayes Estimated Propensities: ", estimatedPropensities

