import numpy
import scipy.optimize
import sys
import scipy.linalg
import scipy.special
import Metrics


def PREDICTED_PROBABILITIES(user_vectors, item_vectors, user_biases, item_biases, global_bias, use_bias = True):
    rawScores = numpy.dot(user_vectors, item_vectors.T)
    probabilities = None
    if use_bias:
        biasedScores = rawScores + user_biases[:,None] + item_biases[None,:] + global_bias
        probabilities = scipy.special.expit(biasedScores)
    else:
        probabilities = scipy.special.expit(rawScores)

    return probabilities

    
def LEARN(observed_ratings, inverse_propensities, l2_regularization, num_dimensions, num_recommendations, bias_mode = 'Regularized',
                    start_vec = None, verbose = False):
    inversePropensities = Metrics.SET_PROPENSITIES(observed_ratings, inverse_propensities, verbose)

    numUsers, numItems = numpy.shape(observed_ratings)
    numObservations = numpy.ma.count(observed_ratings)
    scale = numUsers * numItems

    useBias = None
    regularizeBias = None
    if bias_mode == 'None':
        useBias = False
        regularizeBias = False
    elif bias_mode == 'Regularized':
        useBias = True
        regularizeBias = True
    elif bias_mode == 'Free':
        useBias = True
        regularizeBias = False
    else:
        print "BMF.LEARN: [ERR]\t Bias mode not supported:", bias_mode
        sys.exit(0)
 
    if verbose:
        print "BMF.LEARN: [LOG]\t NumDims:", num_dimensions, "\t NumRecommendations:", num_recommendations,\
                    "\t BiasMode:", bias_mode

    inversePropensities = numpy.ma.filled(inversePropensities, 0.0)
    observedRatings = numpy.ma.filled(observed_ratings, 0)
 
    def Mat2Vec(user_vectors, item_vectors, user_biases, item_biases, global_bias):
        allUserParams = numpy.concatenate((user_vectors, user_biases[:,None]), axis = 1)
        allItemParams = numpy.concatenate((item_vectors, item_biases[:,None]), axis = 1)
        
        allParams = numpy.concatenate((allUserParams, allItemParams), axis = 0)
        paramVector = numpy.reshape(allParams, (numUsers + numItems)*(num_dimensions + 1))
        paramVector = numpy.concatenate((paramVector, [global_bias]))
        return paramVector.astype(numpy.float)
        
    def Vec2Mat(paramVector):
        globalBias = paramVector[-1]
        remainingParams = paramVector[:-1]
        allParams = numpy.reshape(remainingParams, (numUsers + numItems, num_dimensions + 1))
        allUserParams = allParams[0:numUsers,:]
        allItemParams = allParams[numUsers:, :]
        
        userVectors = (allUserParams[:,0:-1]).astype(numpy.longdouble)
        userBiases = allUserParams[:,-1].astype(numpy.longdouble)
        
        itemVectors = allItemParams[:,0:-1].astype(numpy.longdouble)
        itemBiases = allItemParams[:,-1].astype(numpy.longdouble)
        return userVectors, itemVectors, userBiases, itemBiases, globalBias
       
    def Objective(paramVector):
        userVectors, itemVectors, userBiases, itemBiases, globalBias = Vec2Mat(paramVector)
        currentProbabilities = PREDICTED_PROBABILITIES(userVectors, itemVectors, 
                                        userBiases, itemBiases, globalBias, useBias)

        weightedProbabilities = numpy.multiply(inversePropensities, currentProbabilities)
        risk = numpy.multiply(-weightedProbabilities, observedRatings)
        objective = numpy.sum(risk, dtype = numpy.longdouble) / num_recommendations

        gradientProbabilities = numpy.multiply(risk, 1 - currentProbabilities)
        gradientProbabilities = numpy.ma.divide(gradientProbabilities, num_recommendations)

        userVGradient = numpy.dot(gradientProbabilities, itemVectors)
        itemVGradient = numpy.dot(gradientProbabilities.T, userVectors)

        userBGradient = None
        itemBGradient = None
        globalBGradient = None
        if useBias:
            userBGradient = numpy.sum(gradientProbabilities, axis = 1, dtype = numpy.longdouble)
            itemBGradient = numpy.sum(gradientProbabilities, axis = 0, dtype = numpy.longdouble)
            globalBGradient = numpy.sum(gradientProbabilities, dtype = numpy.longdouble)
        else:
            userBGradient = numpy.zeros(numpy.shape(userBiases), dtype = numpy.longdouble)
            itemBGradient = numpy.zeros(numpy.shape(itemBiases), dtype = numpy.longdouble)
            globalBGradient = 0.0
        
        # CARDINALITY CONSTRAINT PER USER        
        itemsPerUser = numpy.mean(currentProbabilities, axis = 1, dtype = numpy.longdouble)
        currentViolations = itemsPerUser - num_recommendations*1.0 / numItems
 
        objective += 0.5 * iterationScale * numpy.sum(numpy.square(currentViolations), dtype = numpy.longdouble)
        objective -= numpy.dot(currentViolations, lagrangeEstimates)
       
        perUserScale = currentViolations * iterationScale - lagrangeEstimates 
        productProbs = numpy.multiply(currentProbabilities, 1 - currentProbabilities)
        gradientProbs = numpy.multiply(perUserScale[:,None], productProbs) / numItems

        userVGradient += numpy.dot(gradientProbs, itemVectors)
        itemVGradient += numpy.dot(gradientProbs.T, userVectors)
        if useBias:
            userBGradient += numpy.sum(gradientProbs, axis = 1, dtype = numpy.longdouble)
            itemBGradient += numpy.sum(gradientProbs, axis = 0, dtype = numpy.longdouble)
            globalBGradient += numpy.sum(gradientProbs, dtype = numpy.longdouble)
        
        # GLOBAL SELF-NORMALIZATION CONSTRAINT
        observedWeightSum = numpy.sum(weightedProbabilities, dtype = numpy.longdouble) / numItems
        observedViolation = observedWeightSum - numUsers*num_recommendations*1.0 / numItems

        objective += 0.5 * iterationScale * observedViolation * observedViolation
        objective -= observedViolation * observedConstraint

        constraintProbs = numpy.multiply(weightedProbabilities, 1 - currentProbabilities)
        constraintProbs *= (iterationScale * observedViolation - observedConstraint) / numItems

        userVGradient += numpy.dot(constraintProbs, itemVectors)
        itemVGradient += numpy.dot(constraintProbs.T, userVectors)
        if useBias:
            userBGradient += numpy.sum(constraintProbs, axis = 1, dtype = numpy.longdouble)
            itemBGradient += numpy.sum(constraintProbs, axis = 0, dtype = numpy.longdouble)
            globalBGradient += numpy.sum(constraintProbs, dtype = numpy.longdouble)
        
        # L2 REGULARIZATION
        l2Lambda = 1.0 * l2Penalty * numUsers / (iterationScale * (numUsers + numItems))
        if regularizeBias:
            l2Lambda /= (num_dimensions + 1)
        else:
            l2Lambda /= num_dimensions

        userVGradient += 2 * l2Lambda * userVectors
        itemVGradient += 2 * l2Lambda * itemVectors

        objective += l2Lambda * numpy.sum(numpy.square(userVectors), dtype = numpy.longdouble)
        objective += l2Lambda * numpy.sum(numpy.square(itemVectors), dtype = numpy.longdouble)

        if regularizeBias:
            userBGradient += 2 * l2Lambda * userBiases
            itemBGradient += 2 * l2Lambda * itemBiases
            globalBGradient += 2 * l2Lambda * globalBias
           
            objective += l2Lambda * numpy.sum(numpy.square(userBiases), dtype = numpy.longdouble)
            objective += l2Lambda * numpy.sum(numpy.square(itemBiases), dtype = numpy.longdouble)
            objective += l2Lambda * globalBias * globalBias

        gradient = Mat2Vec(userVGradient, itemVGradient, userBGradient, itemBGradient, globalBGradient)
        if verbose:
            print ".",
            sys.stdout.flush()

        return objective, gradient
    
    def ObjectiveOnly(paramVector):
        objective, gradient = Objective(paramVector)
        return objective
    def GradientOnly(paramVector):
        objective, gradient = Objective(paramVector)
        return gradient
    
    userVectorsInit = None
    itemVectorsInit = None
    userBiasesInit = None
    itemBiasesInit = None
    globalBiasInit = None
    if start_vec is None:
        userVectorsInit = numpy.random.standard_normal((numUsers, num_dimensions))
        itemVectorsInit = numpy.random.standard_normal((numItems, num_dimensions))
        userBiasesInit = numpy.zeros(numUsers, dtype = numpy.float)
        itemBiasesInit = numpy.zeros(numItems, dtype = numpy.float)
        globalBiasInit = 0
    else:
        userVectorsInit = start_vec[0]
        itemVectorsInit = start_vec[1]
        userBiasesInit = start_vec[2]
        itemBiasesInit = start_vec[3]
        globalBiasInit = start_vec[4]
    
    startVector = Mat2Vec(userVectorsInit, itemVectorsInit, userBiasesInit, itemBiasesInit, globalBiasInit)
           
    ops = {'maxiter': 2000, 'disp': False, 'gtol': 1e-5,\
            'ftol': 1e-5, 'maxcor': 50}
    
    train = True
    originalIterationScale = 1
    l2Penalty = l2_regularization
    while train:
        iterationScale = originalIterationScale             #For scaling up augmented lagrangian violation penalty
        lagrangeEstimates = numpy.zeros(numUsers, dtype = numpy.longdouble)     #For tracking lagrangian estimates
        observedConstraint = 0.0
 
        result = startVector
        feasible = None
        for iterNum in xrange(10):
            #if verbose:
            #    print scipy.optimize.check_grad(ObjectiveOnly, GradientOnly, result)

            result = scipy.optimize.minimize(fun = Objective, x0 = result,
                    method = 'L-BFGS-B', jac = True, tol = 1e-5, options = ops)
    
            #print ""
            #print "BMF.LEARN: [DBG]\t Iter", iterNum," \t Optimization result:", result['message']
            #sys.stdout.flush()
 
            result = result['x']
        
            #Update iterationScale and lagrange estimates
            currentUserVectors, currentItemVectors, currentUserBiases, currentItemBiases, currentGlobalBias =\
                                Vec2Mat(result)
            currentPredictedProbabilities = PREDICTED_PROBABILITIES(currentUserVectors, currentItemVectors, 
                                currentUserBiases, currentItemBiases, currentGlobalBias, useBias)

            currentItemsPerUser = numpy.mean(currentPredictedProbabilities, axis = 1, dtype = numpy.longdouble)
            violations = currentItemsPerUser - num_recommendations*1.0 / numItems

            lagrangeEstimates -= iterationScale * violations
        
            currentWeightedProbabilities = numpy.ma.multiply(currentPredictedProbabilities, inversePropensities)
            currentObservedWeightSum = numpy.ma.sum(currentWeightedProbabilities, dtype = numpy.longdouble) / scale
            currentObservedViolation = currentObservedWeightSum - num_recommendations*1.0 / numItems

            observedConstraint -= iterationScale * currentObservedViolation

            iterationScale *= 5
            feasible = numpy.dot(violations, violations)
            if (iterNum >= 4) and (feasible < 1e-5):
                break

        if feasible < 1e-5:
            train = False
        else:
            userVectorsInit = numpy.random.standard_normal((numUsers, num_dimensions))
            itemVectorsInit = numpy.random.standard_normal((numItems, num_dimensions))
            userBiasesInit = numpy.zeros(numUsers, dtype = numpy.float)
            itemBiasesInit = numpy.zeros(numItems, dtype = numpy.float)
            globalBiasInit = 0
            startVector = Mat2Vec(userVectorsInit, itemVectorsInit, userBiasesInit, itemBiasesInit, globalBiasInit)
            originalIterationScale *= 5
            l2Penalty *= 10

    return Vec2Mat(result)
    
    
if __name__ == "__main__":
    import scipy.sparse
    
    rows = [2,1,4,3,0,4,3]
    cols = [0,2,1,1,0,0,0]
    vals = [1,2,3,4,5,4,5]
    checkY = scipy.sparse.coo_matrix((vals, (rows,cols)), dtype = numpy.int)
    checkY = checkY.toarray()
    checkY = numpy.ma.array(checkY, dtype = numpy.int, mask = checkY <= 0, hard_mask = True, copy = False)
    print "[MAIN]\t Partially observed ratings matrix"
    print checkY

    randomPropensities = numpy.random.random(size = numpy.shape(checkY))
    randomInvPropensities = numpy.reciprocal(randomPropensities)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = LEARN(checkY, None, 5, 1, 
                                                        'Regularized', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = LEARN(checkY, None, 5, 1, 
                                                        'Free', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = LEARN(checkY, None, 5, 1, 
                                                        'None', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = LEARN(checkY, randomInvPropensities, 5, 1, 
                                                        'Free', None, verbose = True)

    print "[MAIN]\t User vectors"
    print userVectors
    print "[MAIN]\t Item vectors"
    print itemVectors
    print "[MAIN]\t User biases"
    print userBiases
    print "[MAIN]\t Item biases"
    print itemBiases
    print "[MAIN]\t Global bias"
    print globalBias
    
    completeScores = PREDICTED_PROBABILITIES(userVectors, itemVectors, userBiases, itemBiases, globalBias, True)
    print "[MAIN]\t Predicted probabilities"
    print completeScores
    
