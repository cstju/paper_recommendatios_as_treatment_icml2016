# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-21 17:03:04
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-22 11:25:48
import numpy 
"""
user_biases = np.array([[1,2,3],[1,2,3],[1,2,3]])
print user_biases
print type(user_biases)
print user_biases[:,None]

x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print x == x[None,:]
"""
"""
import scipy.sparse
    
rows = [2,1,4,3,0,4,3]
cols = [0,2,1,1,0,0,0]
vals = [1,2,3,4,5,4,5]
checkY = scipy.sparse.coo_matrix((vals, (rows,cols)), dtype = numpy.int).toarray()
checkY = numpy.ma.array(checkY, dtype = numpy.int, mask = checkY <= 0, hard_mask = True, copy = False)
inversePropensities = numpy.ones((5,3))
inversePropensities = numpy.ma.array(inversePropensities, dtype = numpy.longdouble, copy = False, 
                            mask = numpy.ma.getmask(checkY), fill_value = 0, hard_mask = True)
print numpy.ma.getmask(checkY)
print inversePropensities
perUserNormalizer = numpy.ma.sum(inversePropensities, axis = 1, dtype = numpy.longdouble)
print perUserNormalizer
perUserNormalizer[3] = -1.0
print perUserNormalizer
perUserNormalizer = numpy.ma.masked_less_equal(perUserNormalizer, 0.0, copy = False)
print perUserNormalizer
"""
"""
x = np.ma.array(checkY, dtype = np.int, mask = checkY <= 0, hard_mask = True, copy = False)
print x[0,1]
print type(x[0,1])

x = np.array([1, 2, 3, -1, 4])
mx = np.ma.masked_array(x, mask=[0, 0, 0, 1, 0])
print mx.mean()
"""
"""
x1 = numpy.arange(9.0).reshape((3,3))
x2 = numpy.arange(3.0)
print x1
print x2[:,None]
print x2[None,:]
print numpy.divide(x1,x2[:,None])
print numpy.divide(x1,x2[None,:])
"""
a = numpy.array([[1, 2], [3, 4]])
b = numpy.array([5])
print numpy.multiply(a,a)