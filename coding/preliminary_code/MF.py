import numpy
import scipy.optimize
import sys
import Metrics


def PREDICTED_SCORES(user_vectors, item_vectors, user_biases, item_biases, global_bias, use_bias = True):
    #采用矩阵分解的方法获得用户对商品的预测评分
    rawScores = numpy.dot(user_vectors, item_vectors.T)#直接用用户特征向量和商品特征向量相乘得到预测评分
    if use_bias:#引入用户偏差和商品偏差
        biasedScores = rawScores + user_biases[:,None] + item_biases[None,:] + global_bias
        return biasedScores
    else:
        return rawScores

    
def GENERATE_MATRIX(observed_ratings, inverse_propensities, l2_regularization, num_dimensions, normalization,
        bias_mode = 'Regularized', mode = 'MSE', start_vec = None, verbose = False):

    metricMode = None#计算误差的方法，在函数Objective中会用到
    if mode == 'MSE':
        metricMode = 1
    elif mode == 'MAE':
        metricMode = 2
    else:
        print "MF.GENERATE_MATRIX: [ERR]\t Metric not supported:", mode
        sys.exit(0)

    #这里的倒数偏置矩阵已经将观测矩阵中缺省值对应的偏置值也设为缺省了
    inversePropensities = Metrics.SET_PROPENSITIES(observed_ratings, inverse_propensities, False)

    numUsers, numItems = numpy.shape(observed_ratings)#获得用户总数和商品总数
    scale = numUsers * numItems
    numObservations = numpy.ma.count(observed_ratings)#获得观测矩阵中有值的评分个数

    #按照行对倒数偏置矩阵求和，作为每个用户的偏置总计
    perUserNormalizer = numpy.ma.sum(inversePropensities, axis = 1, dtype = numpy.longdouble)
    #perUserNormalizer中小于0.0的值标记出
    perUserNormalizer = numpy.ma.masked_less_equal(perUserNormalizer, 0.0, copy = False)

    #按照列对倒数偏置矩阵求和，作为每个商品的偏置总计
    perItemNormalizer = numpy.ma.sum(inversePropensities, axis = 0, dtype = numpy.longdouble)
    #perItemNormalizer中小于0.0的值标记出
    perItemNormalizer = numpy.ma.masked_less_equal(perItemNormalizer, 0.0, copy = False)

    globalNormalizer = numpy.ma.sum(inversePropensities, dtype = numpy.longdouble)#偏置矩阵总和

    normalizedPropensities = None
    if normalization == 'Vanilla':
        normalizedPropensities = inversePropensities
    elif normalization == 'SelfNormalized':
        #numpy.ma.divide是矩阵除运算，与numpy.divide一样，只不过不对缺省值进行运算
        normalizedPropensities = scale * numpy.ma.divide(inversePropensities, globalNormalizer)
    elif normalization == 'UserNormalized':
        #perUserNormalizer[:, None]是一个列向量
        #inversePropensities的每一列分别与perUserNormalizer[:, None]做除法，再乘以商品总数，对应用户偏置的归一化
        normalizedPropensities = numItems * numpy.ma.divide(inversePropensities, perUserNormalizer[:, None])
    elif normalization == 'ItemNormalized':
        #perItemNormalizer[None, :]是一个行向量
        #inversePropensities的每一行分别与perItemNormalizer[None, :]做除法，再乘以用户总数，对应商品偏置的归一化
        normalizedPropensities = numUsers * numpy.ma.divide(inversePropensities, perItemNormalizer[None, :])
    else:
        print "MF.GENERATE_MATRIX: [ERR]\t Normalization not supported:", normalization
        sys.exit(0)#终止程序，单纯的退出
    
    useBias = None
    regularizeBias = None
    if bias_mode == 'None':
        useBias = False
        regularizeBias = False
    elif bias_mode == 'Regularized':
        useBias = True
        regularizeBias = True
    elif bias_mode == 'Free':
        useBias = True
        regularizeBias = False
    else:
        print "MF.GENERATE_MATRIX: [ERR]\t Bias mode not supported:", bias_mode
        sys.exit(0)

    if verbose:
        print "MF.GENERATE_MATRIX: [LOG]\t Lamda:", l2_regularization, "\t NumDims:", num_dimensions,\
            "\t Normalization:", normalization, "\t Metric:", mode, "\t BiasMode:", bias_mode

    normalizedPropensities = numpy.ma.filled(normalizedPropensities, 0.0)
    observedRatings = numpy.ma.filled(observed_ratings, 0)#将观测评分中的缺省值填充为0
    
    def Mat2Vec(user_vectors, item_vectors, user_biases, item_biases, global_bias):#将各种参数整合
        #将用户偏置加到每个用户特征向量（行向量）的最后，user_biases[:,None]是一个列向量
        allUserParams = numpy.concatenate((user_vectors, user_biases[:,None]), axis = 1)
        #将商品偏置加到每个商品特征向量（行向量）的最后
        allItemParams = numpy.concatenate((item_vectors, item_biases[:,None]), axis = 1)
        
        #将整个商品参数接到用户参数的后面
        allParams = numpy.concatenate((allUserParams, allItemParams), axis = 0)

        #将参数矩阵从新整合，行数为用户总数加商品总数，列数为维数加1
        paramVector = numpy.reshape(allParams, (numUsers + numItems)*(num_dimensions + 1))
        paramVector = numpy.concatenate((paramVector, [global_bias]))
        return paramVector.astype(numpy.float)
        
    def Vec2Mat(paramVector):#将各种参数拆分
        globalBias = paramVector[-1]
        remainingParams = paramVector[:-1]
        allParams = numpy.reshape(remainingParams, (numUsers + numItems, num_dimensions + 1))
        allUserParams = allParams[0:numUsers,:]
        allItemParams = allParams[numUsers:, :]
        
        userVectors = (allUserParams[:,0:-1]).astype(numpy.longdouble)
        userBiases = (allUserParams[:,-1]).astype(numpy.longdouble)
        
        itemVectors = (allItemParams[:,0:-1]).astype(numpy.longdouble)
        itemBiases = (allItemParams[:,-1]).astype(numpy.longdouble)
        return userVectors, itemVectors, userBiases, itemBiases, globalBias
    
    def Objective(paramVector):
        userVectors, itemVectors, userBiases, itemBiases, globalBias = Vec2Mat(paramVector)
        biasedScores = PREDICTED_SCORES(userVectors, itemVectors, userBiases, itemBiases, globalBias, useBias)

        delta = numpy.subtract(biasedScores, observedRatings)
        loss = None
        if metricMode == 1:
            loss = numpy.square(delta)
        elif metricMode == 2:
            loss = numpy.abs(delta)
        else:
            sys.exit(0)
        #numpy.multiply是矩阵点乘
        weightedLoss = numpy.multiply(loss, normalizedPropensities)
        objective = numpy.sum(weightedLoss, dtype = numpy.longdouble)

        gradientMultiplier = None
        if metricMode == 1:
            gradientMultiplier = numpy.multiply(normalizedPropensities, 2 * delta)
        elif metricMode == 2:
            gradientMultiplier = numpy.zeros(numpy.shape(delta), dtype = numpy.int)
            gradientMultiplier[delta > 0] = 1
            gradientMultiplier[delta < 0] = -1
            gradientMultiplier = numpy.multiply(normalizedPropensities, gradientMultiplier)
        else:
            sys.exit(0)

        userVGradient = numpy.dot(gradientMultiplier, itemVectors)
        itemVGradient = numpy.dot(gradientMultiplier.T, userVectors)

        userBGradient = None
        itemBGradient = None
        globalBGradient = None
        if useBias:
            userBGradient = numpy.sum(gradientMultiplier, axis = 1, dtype = numpy.longdouble)
            itemBGradient = numpy.sum(gradientMultiplier, axis = 0, dtype = numpy.longdouble)
            globalBGradient = numpy.sum(gradientMultiplier, dtype = numpy.longdouble)
        else:
            userBGradient = numpy.zeros(numpy.shape(userBiases), dtype = numpy.longdouble)
            itemBGradient = numpy.zeros(numpy.shape(itemBiases), dtype = numpy.longdouble)
            globalBGradient = 0.0

        if l2_regularization > 0:
            scaledPenalty = 1.0 * l2_regularization * scale / (numUsers + numItems)
            if regularizeBias:
                scaledPenalty /= (num_dimensions + 1)
            else:
                scaledPenalty /= num_dimensions

            userVGradient += 2 * scaledPenalty * userVectors
            itemVGradient += 2 * scaledPenalty * itemVectors
          
            objective += scaledPenalty * numpy.sum(numpy.square(userVectors), dtype = numpy.longdouble)
            objective += scaledPenalty * numpy.sum(numpy.square(itemVectors), dtype = numpy.longdouble)
 
            if regularizeBias:
                userBGradient += 2 * scaledPenalty * userBiases
                itemBGradient += 2 * scaledPenalty * itemBiases
                globalBGradient += 2 * scaledPenalty * globalBias
                objective += scaledPenalty * numpy.sum(numpy.square(userBiases), dtype = numpy.longdouble)
                objective += scaledPenalty * numpy.sum(numpy.square(itemBiases), dtype = numpy.longdouble)
                objective += scaledPenalty * globalBias * globalBias
            
        gradient = Mat2Vec(userVGradient, itemVGradient, userBGradient, itemBGradient, globalBGradient)

        if verbose:
            print ".",
            sys.stdout.flush()
        
        return objective, gradient
    
    def ObjectiveOnly(paramVector):
        objective, gradient = Objective(paramVector)
        return objective
    def GradientOnly(paramVector):
        objective, gradient = Objective(paramVector)
        return gradient
    
    userVectorsInit = None
    itemVectorsInit = None
    userBiasesInit = None
    itemBiasesInit = None
    globalBiasInit = None
    if start_vec is None:#各个向量和参数的初始值
        userVectorsInit = numpy.random.standard_normal((numUsers, num_dimensions))#构建均值为0，标准差为1的用户特征矩阵
        itemVectorsInit = numpy.random.standard_normal((numItems, num_dimensions))
        userBiasesInit = numpy.zeros(numUsers, dtype = numpy.float)
        itemBiasesInit = numpy.zeros(numItems, dtype = numpy.float)
        globalBiasInit = 0
    else:
        userVectorsInit = start_vec[0]
        itemVectorsInit = start_vec[1]
        userBiasesInit = start_vec[2]
        itemBiasesInit = start_vec[3]
        globalBiasInit = start_vec[4]
    
    #将各个分开的参数整合到一起
    startVector = Mat2Vec(userVectorsInit, itemVectorsInit, userBiasesInit, itemBiasesInit, globalBiasInit)

    if verbose:
        print "MF.GENERATE_MATRIX: [DBG]\t Checking gradients"
        print scipy.optimize.check_grad(ObjectiveOnly, GradientOnly, startVector)

    ops = {'maxiter': 2000, 'disp': False, 'gtol': 1e-5,\
            'ftol': 1e-5, 'maxcor': 50}

    #这个函数好，给出规则，给出初始值，给出参数就可以迭代
    result = scipy.optimize.minimize(fun = Objective, x0 = startVector,
                    method = 'L-BFGS-B', jac = True, tol = 1e-5, options = ops)
    
    if verbose:
        print ""
        print "MF.GENERATE_MATRIX: [DBG]\t Optimization result:", result['message']
        sys.stdout.flush()#立刻输出，没有缓冲

    return Vec2Mat(result['x'])
    
    
if __name__ == "__main__":
    #这里的主函数是用于检验上面程序编写是否正确而创建的，没用使用真实的数据集，只构建了一个很小的矩阵用于验证
    import scipy.sparse
    
    rows = [2,1,4,3,0,4,3]
    cols = [0,2,1,1,0,0,0]
    vals = [1,2,3,4,5,4,5]
    #对于系数矩阵，采用scipy.sparse.coo_matrix函数能更快的构建矩阵
    #rows为对应的vals的横坐标，cols为对应的vals的纵坐标
    checkY = scipy.sparse.coo_matrix((vals, (rows,cols)), dtype = numpy.int)
    checkY = checkY.toarray()#转化为数组形式
    checkY = numpy.ma.array(checkY, dtype = numpy.int, mask = checkY <= 0, hard_mask = True, copy = False)
    # mask : sequence, optional
    #       Mask. Must be convertible to an array of booleans with the same shape as data.
    #       True indicates a masked (i.e. invalid) data.
    # hard_mask: Whether to use a hard mask or not. With a hard mask, masked values cannot be unmasked. Default is False.
    """
    使用numpy.ma.array函数将元矩阵中为0的缺省评分标记为空缺，空缺评分不会参与任何计算
    例如
    >>> x = np.array([1, 2, 3, -1, 4])
    >>> mx = np.ma.masked_array(x, mask=[0, 0, 0, 1, 0])#将第四个元素设为无效值
    >>> print mx.mean()
    >>> 2.5
    >>> print mx[3]
    >>> --  #没有显示数值，不为空，也不是0，用--代表空缺
    """
    print "[MAIN]\t Partially observed ratings matrix"
    print checkY

    randomPropensities = numpy.random.random(size = numpy.shape(checkY))#随机初始偏置
    randomInvPropensities = numpy.reciprocal(randomPropensities)#计算偏置的倒数

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, None, 1.0, 5, 'Vanilla', 'Regularized', 'MSE', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, randomInvPropensities, 1.0, 5, 'Vanilla', 'Regularized', 'MSE', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, randomInvPropensities, 1.0, 5, 'Vanilla', 'Regularized', 'MAE', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, randomInvPropensities, 1.0, 5, 'SelfNormalized', 'Regularized', 'MSE', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, None, 1.0, 5, 'Vanilla', 'Free', 'MSE', None, verbose = True)

    userVectors, itemVectors, userBiases, itemBiases, globalBias = \
            GENERATE_MATRIX(checkY, randomInvPropensities, 1.0, 5, 'SelfNormalized', 'None', 'MSE', None, verbose = True)


    print "[MAIN]\t User vectors"
    print userVectors
    print "[MAIN]\t Item vectors"
    print itemVectors
    print "[MAIN]\t User biases"
    print userBiases
    print "[MAIN]\t Item biases"
    print itemBiases
    print "[MAIN]\t Global bias"
    print globalBias
    
    completeScores = PREDICTED_SCORES(userVectors, itemVectors, userBiases, itemBiases, globalBias, True)#得到全评分矩阵
    print "[MAIN]\t Predicted scores"
    print completeScores
    
